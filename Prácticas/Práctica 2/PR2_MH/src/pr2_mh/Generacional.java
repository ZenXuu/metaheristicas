package pr2_mh;

import static java.lang.Math.random;
import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;
import java.util.Vector;

/**
 *
 * @author admin
 */
public class Generacional {
    private Vector<Individuo> poblacion;
    private int tam_poblacion;
    private int tam_sol;
    private Random random;
    private int generacion;
    private int[][] distancias;
    private int[][] flujos;
    private int k;
    private String cruce;
    private int evaluaciones;
    private ArrayList<Individuo> elites;
    private StringBuilder log;
    
    public Generacional(int _tam, long _semilla, int[][] _distancias, int[][] _flujos, int _k, String _cruce, StringBuilder _log) {
        this.tam_poblacion = 50;
        this.tam_sol = _tam;
        this.random = new Random();
        this.random.setSeed(_semilla);
        this.generacion = 0;
        this.distancias = _distancias;
        this.flujos = _flujos;
        this.k = _k;
        this.cruce = _cruce;
        this.evaluaciones = 0;
        this.elites = new ArrayList<>();
        this.log = _log;
    }
    
    public int ejecutar(){
        int maxEvaluaciones = 50000;
        //1- Generar poblacion
        this.poblacion = generaPoblacion();
        while(evaluaciones < maxEvaluaciones){ 
            log.append("Generacion: " + generacion + "\n");
            generacion++;
            //2- Evaluar poblacion
            evaluarPoblacion(poblacion);
            actualizaElites();
            
            //3- Seleccion
            Vector<Individuo> pobAux = new Vector<>();
            seleccionPoblacion(pobAux);
            pobAux.clear();
            
            if(generacion == 1 || generacion == 2){
                log.append("Poblacion pre-Recombinacion: \n");
                guardaPoblacionLog(poblacion);
            }
            //4- Recombinacion
            recombinacion(pobAux);
            
            //5- Evaluar poblacion
            evaluarPoblacion(pobAux);
            
            if(generacion == 1 || generacion == 2){
                log.append("Poblacion post-Recombinacion: \n");
                guardaPoblacionLog(pobAux);
            }
            
            //6- Reemplazar poblacion
            reemplazarPoblacion(pobAux);                                                                                                                                
            //Elitismo
            anadeElites();//reemplazamos elites
        }
        System.out.println("MejorCoste: " + mejorIndPoblacion());
        return mejorIndPoblacion();
    }
    
    private void recombinacion(Vector<Individuo> pobAux){
        if(cruce == "MOC"){
                int i = 0; 
                while(i < tam_poblacion){
                    ArrayList<Integer> padres = torneoBinario();
                    if(random.nextFloat() <= 0.7){
                        try{
                            ArrayList<int[]> descendientes = MOC(poblacion.get(padres.get(0)).getSolucion(), poblacion.get(padres.get(1)).getSolucion());
                        
                            int[] s1 = descendientes.get(0), s2 = descendientes.get(1);
                            for(int j = 0; j < tam_sol; j++){
                                if(random.nextFloat() <= 0.05){
                                    s1 = tresOpt(descendientes.get(0), j);
                                }
                            }
                            for(int j = 0; j < tam_sol; j++){
                                if(random.nextFloat() <= 0.05){
                                    s2 = tresOpt(descendientes.get(1), j);
                                }
                            }
                            Individuo i1 = new Individuo(s1, generacion);
                            Individuo i2 = new Individuo(s2, generacion);
                            pobAux.add(i1);
                            pobAux.add(i2);
                        i += 2;
                        }catch(Exception e){
                            System.out.println(poblacion.size());
                        }
                    }
                }
            }
        if(cruce == "OX2"){
            int i = 0; 
            while(i < tam_poblacion){
                ArrayList<Integer> padres = torneoBinario();
                if(random.nextFloat() <= 0.7){
                    int[] hijo = OX2(poblacion.get(padres.get(0)).getSolucion(), poblacion.get(padres.get(1)).getSolucion());
                    int[] solHijo = hijo;
                    for(int j = 0; j < tam_sol; j++){
                        if(random.nextFloat() <= 0.05){
                            solHijo = tresOpt(hijo, j);
                        }
                    }
                    Individuo i1 = new Individuo(solHijo, generacion);
                    pobAux.add(i1);
                    int[] hijo2 = OX2(poblacion.get(padres.get(1)).getSolucion(), poblacion.get(padres.get(0)).getSolucion());
                    int[] solHijo2 = hijo2;
                    for(int j = 0; j < tam_sol; j++){
                        if(random.nextFloat() <= 0.05){
                            solHijo2 = tresOpt(hijo, j);
                        }
                    }
                    Individuo i2 = new Individuo(solHijo2, generacion);
                    pobAux.add(i2);
                    i += 2;
                }
            }
        }
    }
    private void guardaPoblacionLog(Vector<Individuo> pob){
        for(int i = 0; i < tam_poblacion; i++){
            log.append("Individuo " + i + ": ");
            Individuo ind = pob.get(i);
            for(int j = 0; j < tam_sol; j++){
                log.append(ind.getSolucion()[j] + ", ");
            }
            log.append(" Coste: " + ind.getCoste() + "\n");
        }
    }
    
    private void reemplazarPoblacion(Vector<Individuo> pobAux){
        poblacion.clear();
        for(int i = 0; i < tam_poblacion; i++){
            poblacion.add(pobAux.get(i));
        }
    }
 
    void anadeElites(){
        if(k == 2){
            ArrayList<Integer> posPeores = dosPeores();
            poblacion.set(posPeores.get(0), elites.get(0));
            poblacion.set(posPeores.get(1), elites.get(1));
            log.append("Coste elite 1: " + elites.get(0).getCoste() + "\n");
            log.append("Coste elite 2: " + elites.get(1).getCoste() + "\n");
        }
        if(k == 3){
            ArrayList<Integer> posPeores = tresPeores();
            poblacion.set(posPeores.get(0), elites.get(0));
            poblacion.set(posPeores.get(1), elites.get(1));
            poblacion.set(posPeores.get(2), elites.get(2));
            log.append("Coste elite 1: " + elites.get(0).getCoste() + "\n");
            log.append("Coste elite 2: " + elites.get(1).getCoste() + "\n");
            log.append("Coste elite 3: " + elites.get(2).getCoste() + "\n");
        }
        log.append("------------------------------" + "\n");
    }
    
    private void actualizaElites(){
        if(k == 2)
            elites = dosMejores();
        if(k == 3)
            elites = tresMejores();
    }
    
    private void seleccionPoblacion(Vector<Individuo> pobAux){
        for(int i = 0; i < tam_poblacion; i++){
            pobAux.add(seleccion());
        }
        poblacion.clear();
        for(int i = 0; i < tam_poblacion; i++){
            poblacion.add(pobAux.get(i));
        }
    }
    /**
     * @brief evalua la poblacion pasada como parametro, dandole valor al coste de cada uno de sus individuos
     * @param pob [in] 
     */
    private void evaluarPoblacion(Vector<Individuo> pob){
        for(int i = 0; i < tam_poblacion; i++){
            Individuo ind = pob.get(i);
            if(!ind.getEvaluado()){
                ind.setCoste(evaluacion(ind.getSolucion(), distancias, flujos));
                ind.setEvaluado(true);
                evaluaciones++;
            }
        }
    }
    /**
     * @return el mejor individuo de la poblacion
     */
    private int mejorIndPoblacion(){
        int mejorCoste = Integer.MAX_VALUE;
        for(int i = 0; i < tam_poblacion; i++){
            int costeInd = poblacion.get(i).getCoste();
            if( costeInd < mejorCoste){
                mejorCoste = costeInd;
            }
        }
        return mejorCoste;
    }
    /**
     * @brief funcion del torneo binario en la cual se cogen aleatoriamente dos posiciones diferentes de la poblacion
     * @return array con las dos posiciones
     */
    private ArrayList<Integer> torneoBinario(){
        int pos1 = random.nextInt(tam_poblacion);
        int pos2 = random.nextInt(tam_poblacion);
        if(pos2 == pos1){
            while(pos2 == pos1){
                pos2 = random.nextInt(tam_poblacion);
            }
        }
        ArrayList<Integer> pos = new ArrayList<>();
        pos.add(pos1);
        pos.add(pos2);
        return pos;
    }
    /**
     * @brief funcion del operador de cruce MOC
     * @param p1 padre 1
     * @param p2 padre 2
     * @return array de los dos hijos producidos tras el cruce
     */
    private ArrayList<int[]> MOC(int[] p1, int[] p2){
        int[] h1 = new int[tam_sol], h2 = new int[tam_sol];
        ArrayList<int[]> hijos = new ArrayList<>();
        for(int i = 0; i < tam_sol; i++){
            h1[i] = p1[i];
            h2[i] = p2[i];
        }
        int punto = 1 + random.nextInt(tam_sol - 2);
        for(int i = punto; i < tam_sol; i++){
            int aux = p1[i];
            int aux2 = p2[i];
            for(int j = 0; j < tam_sol; j++){
                if(h2[j] == aux){
                    h2[j] = -1;
                }
                if(h1[j] == aux2){
                    h1[j] = -1;
                }
            }
        }
        int pos1 = punto, pos2 = punto;
        for(int i = 0; i < tam_sol; i++){
            if(h1[i] == -1){
                h1[i] = p2[pos2++];
            }
            if(h2[i] == -1){
                h2[i] = p1[pos1++];
            }
        }
        hijos.add(h1);
        hijos.add(h2);
        return hijos;
    }
    /**
     * @brief operador de cruce OX2
     * @param p1 primer padre 
     * @param p2 segundo padre
     * @return devuelve la solucion tras aplicar el cruce
     */
    private int[] OX2(int[] p1, int[] p2){
        ArrayList<Integer> seleccionados = new ArrayList<>();
        for(int i = 0; i < tam_sol; i++){
            float prob = random.nextFloat();
            if(prob < 0.5){
                seleccionados.add(i);
            }
        }
        int[] hijo = new int[tam_sol];
        for(int i = 0; i < tam_sol; i++){
            hijo[i] = p2[i];
        }
        for(int i = 0; i < seleccionados.size(); i++){
            int n = p1[seleccionados.get(i)];
            for(int j = 0; j < tam_sol; j++){
                if(hijo[j] == n){
                    hijo[j] = -1;
                }
            }
        }
        for(int i = 0; i < tam_sol; i++){
            if(hijo[i] == -1){
                hijo[i] = p1[seleccionados.remove(0)];
            }
        }
        return hijo;
    }
    /**
     * @brief operador 3opt
     * @param solucion [in][out] solucion a la cual aplicar el operador
     * @param i posicion inicial de la cual se empieza a hacer el 3opt, se seleccionan otros dos indices aleatoriamente
     * @return solucion tras aplicarle el operador 3opt
     */
    private int[] tresOpt(int[] solucion, int i){
        int nuevaSol[] = new int[tam_sol];
        for(int ii = 0; ii < tam_sol; ii++){
            nuevaSol[ii] = solucion[ii];
        }
        
        int j = random.nextInt(tam_sol);
        if(j == i){
            while(j == i){
                j = random.nextInt(tam_sol);
            }
        }
        int k = random.nextInt(tam_sol);
        if(k == i || k == j){
            while(k == i || k == j){
                k = random.nextInt(tam_sol);
            }
        }
        //System.out.println("i: " + i + " j: " + j + " k: " + k); 
        int aux = solucion[i];
        nuevaSol[i] = solucion[k];
        int aux2 = solucion[j];
        nuevaSol[j] = aux;
        nuevaSol[k] = aux2;
        return nuevaSol;
    }
    /**
     * @brief funcion de seleecion de individuos por torneo aleatorio 
     * @return devuelve el individuo seleccionado
     */
    private Individuo seleccion(){
        int pos1 = random.nextInt(tam_sol);
        int pos2 = random.nextInt(tam_sol);
        if(pos2 == pos1){
            while(pos2 == pos1){
                pos2 = random.nextInt(tam_sol);
            }
        }
        Individuo ind1 = this.poblacion.get(pos1);
        Individuo ind2 = this.poblacion.get(pos2);

        int c1 = ind1.getCoste(), c2 = ind2.getCoste();
        //System.out.println("Pos ind1: " + pos1 + " Coste: " + c1 + " Pos ind2: " + pos2 + " Coste: " + c2);
        if(c1 < c2){
            return ind1;
        }else{
            return ind2;
        }
    }
    /**
     * @brief genera una poblacion de tamanio tam_poblacion
     * @return la poblacion generada
     */
    private Vector<Individuo> generaPoblacion(){
        Vector<Individuo> pob = new Vector<>();
        for(int i = 0; i < tam_poblacion; i++){
            pob.add(generaIndividuo());
        }
        return pob;
    }
    /**
     * @brief genera de forma aleatoria un individuo
     * @return el individuo generado
     */
    private Individuo generaIndividuo(){
        int[] sol = new int[tam_sol];
        ArrayList<Integer> listaPosiblesSol = new ArrayList<Integer>();
        for(int i = 0; i < tam_sol; i++){
            listaPosiblesSol.add(i);
        }
        int tamLista = tam_sol;
        int tamSol = 0;
        while(tamSol != tam_sol){
            int loc = random.nextInt(tamLista);
            if(listaPosiblesSol.contains(loc)){
                sol[tamSol++] = loc;
                listaPosiblesSol.remove((Integer)loc);
            }
        }
        Individuo ind = new Individuo(sol, generacion);
        return ind;
    }
    /**
     * @brief muestra por pantalla una poblacion
     * @param pob [in] la poblacion que va a ser visualizada
     */
    private void visualizaPoblacion(Vector<Individuo> pob){
        for(int i = 0; i < pob.size(); i++){
            print(pob.get(i).getSolucion());
            System.out.println(" ----> Coste: " + pob.get(i).getCoste() + " Posicion: " + i);
        }
    }
    //muestra por pantalla una solucion
    private void print(int[] sol){
        for(int i = 0; i < tam_sol - 1; i++){
            System.out.print(sol[i] + ", ");
        }
        System.out.print(sol[tam_sol - 1]);
    }
    
    /**
     * @brief funcion que devuelve la posicion de los tres mejores individuos de la poblacion
     * @return la posicion de los tres mejores individuos de la poblacion
     */
    private ArrayList<Individuo> tresMejores(){
        int mejorCoste1 = Integer.MAX_VALUE, mejorCoste2 = Integer.MAX_VALUE, mejorCoste3 = Integer.MAX_VALUE;
        int p1 = 0, p2 = 0, p3 = 0;
        for(int i = 0; i < tam_poblacion; i++){
            Individuo ind = poblacion.get(i);
            if(ind.getCoste() < mejorCoste1){
                mejorCoste3 = mejorCoste2;
                mejorCoste2 = mejorCoste1;
                p3 = p2;
                p2 = p1;
                
                p1 = i;
                mejorCoste1 = ind.getCoste();
            }else{
                if(ind.getCoste() < mejorCoste2){
                    mejorCoste3 = mejorCoste2;
                    p3 = p2;
                    p2 = i;
                    mejorCoste2 = ind.getCoste();
                }else{
                    if(ind.getCoste() < mejorCoste3){
                        p3 = i;
                        mejorCoste3 = ind.getCoste();
                    }
                }
            }
        }
        ArrayList<Individuo> mejores = new ArrayList<>();
        mejores.add(poblacion.get(p1));
        mejores.add(poblacion.get(p2));
        mejores.add(poblacion.get(p3));
        return mejores;
    }
    /**
     * @brief funcion que devuelve la posicion de los tres peores individuos de la poblacion
     * @return la posicion de los tres peores individuos de la poblacion
     */
    private ArrayList<Integer> tresPeores(){
        int mejorCoste1 = Integer.MIN_VALUE, mejorCoste2 = Integer.MIN_VALUE, mejorCoste3 = Integer.MIN_VALUE;
        int p1 = 0, p2 = 0, p3 = 0;
        for(int i = 0; i < tam_poblacion; i++){
            Individuo ind = poblacion.get(i);
            if(ind.getCoste() > mejorCoste1){
                mejorCoste3 = mejorCoste2;
                mejorCoste2 = mejorCoste1;
                p3 = p2;
                p2 = p1;
                
                p1 = i;
                mejorCoste1 = ind.getCoste();
            }else{
                if(ind.getCoste() > mejorCoste2){
                    mejorCoste3 = mejorCoste2;
                    p3 = p2;
                    p2 = i;
                    mejorCoste2 = ind.getCoste();
                }else{
                    if(ind.getCoste() > mejorCoste3){
                        p3 = i;
                        mejorCoste3 = ind.getCoste();
                    }
                }
            }
        }
        ArrayList<Integer> peores = new ArrayList<>();
        peores.add(p1);
        peores.add(p2);
        peores.add(p3);
        return peores;
    }
    
    /**
     * @brief funcion que devuelve la posicion de los dos mejores individuos de la poblacion
     * @return la posicion de los dos mejores individuos de la poblacion
     */
    private ArrayList<Individuo> dosMejores(){
        int mejorCoste1 = Integer.MAX_VALUE, mejorCoste2 = Integer.MAX_VALUE;
        
        int m1 = 0, m2 = 0;
        for(int i = 0; i < tam_poblacion; i++){
            Individuo ind = poblacion.get(i);
            if(ind.getCoste() < mejorCoste1){
                mejorCoste2 = mejorCoste1;
                m2 = m1;
                mejorCoste1 = ind.getCoste();
                m1 = i;
            }else{
                if(ind.getCoste() < mejorCoste2){
                    m2 = i;
                    mejorCoste2 = ind.getCoste();
                }
            }
        }
        
        ArrayList<Individuo> mejores = new ArrayList<>();
        mejores.add(poblacion.get(m1));
        mejores.add(poblacion.get(m2));
        
        return mejores;
    }
    /**
     * @brief funcion que devuelve la posicion de los dos peores individuos de la poblacion
     * @return la posicion de los dos peores individuos de la poblacion
     */
    private ArrayList<Integer> dosPeores(){
        int peorCoste1 = Integer.MIN_VALUE, peorCoste2 = Integer.MIN_VALUE;
        ArrayList<Integer> mejores = new ArrayList<>();
        int p1 = 0, p2 = 0;
        for(int i = 0; i < tam_poblacion; i++){
            Individuo ind = poblacion.get(i);
            if(ind.getCoste() > peorCoste1){
                peorCoste2 = peorCoste1;
                p2 = p1;
                peorCoste1 = ind.getCoste();
                p1 = i;
            }else{
                if(ind.getCoste() > peorCoste2){
                    p2 = i;
                    peorCoste2 = ind.getCoste();
                }
            }
        }
        mejores.add(p1);
        mejores.add(p2);
        
        return mejores;
    }
     
    private void traspuesta(int mat[][], int tr[][]){
        for (int i = 0; i < tam_sol; i++) 
            for (int j = 0; j < tam_sol; j++) 
                tr[i][j] = mat[j][i]; 
    }
    
    private boolean esSimetrica(int mat[][]){
        int[][] trasp = new int[tam_sol][tam_sol];
        traspuesta(mat, trasp);
        
        for(int i = 0; i < tam_sol; i++){
            for(int j = 0; j < tam_sol; j++){
                if(mat[i][j] != trasp[i][j])
                    return false;
            }
        }
        return true;
    }
    
    public int evaluacion(int[] resultado, int[][] mD, int[][] mF) {
        int suma = 0;
        if(!esSimetrica(mF)){    
            for (int i = 0; i < mF.length; i++) {
                for (int j = 0; j < mF.length; j++) {
                    suma += (mF[i][j] * mD[resultado[i]][resultado[j]]);
                }
            }
        }else{
            for(int i = 0; i < mF.length; i++){
                for(int  j = i; j < mF.length; j++){
                    suma += (2 * (mF[i][j] * mD[resultado[i]][resultado[j]]));
                    
                }
            }
        }
        return suma;
    }
}



