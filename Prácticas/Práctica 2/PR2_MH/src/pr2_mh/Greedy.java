/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr2_mh;

/**
 *
 * @author migue
 */
public class Greedy {
    private int tam;
    private int[][] flujos;
    private int[][] distancias;
    private StringBuilder log;
    
    public Greedy(int _tam, int[][] _flujos, int[][] _distancias, StringBuilder _log) {
        tam = _tam;
        flujos = _flujos;
        distancias = _distancias;
        log = _log;
    }
    
    public int ejecutar(){
        int fsuma[] = new int[tam];
        int dsuma[] = new int[tam];
        for(int i = 0; i < tam; i++){
            for(int j = 0; j < tam; j++){
                fsuma[i] += flujos[i][j];
                dsuma[i] += distancias[i][j];
            }
        }
        
        int solucion[] = new int[tam];
        int tamSol = 0;
        while(tamSol != tam){
            int mayorFlujo = 0, posMayorFlujo = -1;
            for(int i = 0; i < tam; i++){
                if(fsuma[i] > mayorFlujo){
                    mayorFlujo = fsuma[i];
                    posMayorFlujo = i;
                }
            }
            fsuma[posMayorFlujo] = -1;
            
            int menorDistancia = Integer.MAX_VALUE, posMenorDistancia = -1;
            for(int i = 0; i < tam; i++){
                if(dsuma[i] < menorDistancia){
                    menorDistancia = dsuma[i];
                    posMenorDistancia = i;
                }
            }
            dsuma[posMenorDistancia] = Integer.MAX_VALUE;
            
            solucion[posMayorFlujo] = posMenorDistancia;
            tamSol++;
        }
        
        log.append("Solucion: ");
        for(int i = 0; i < tam - 1; i++){
            log.append(solucion[i] + ", ");
        }
        log.append(solucion[tam-1]);
        int CosteG = evaluacion(solucion, distancias, flujos);
        log.append("\nEl coste es: " + CosteG);
        
        return CosteG; 
        
    }
    
     
     private void traspuesta(int mat[][], int tr[][]){
        for (int i = 0; i < tam; i++) 
            for (int j = 0; j < tam; j++) 
                tr[i][j] = mat[j][i]; 
    }
    
    private boolean esSimetrica(int mat[][]){
        int[][] trasp = new int[tam][tam];
        traspuesta(mat, trasp);
        
        for(int i = 0; i < tam; i++){
            for(int j = 0; j < tam; j++){
                if(mat[i][j] != trasp[i][j])
                    return false;
            }
        }
        return true;
    }
    
    public int evaluacion(int[] resultado, int[][] mD, int[][] mF) {
        int suma = 0;
        if(!esSimetrica(mF)){    
            for (int i = 0; i < mF.length; i++) {
                for (int j = 0; j < mF.length; j++) {
                    suma += (mF[i][j] * mD[resultado[i]][resultado[j]]);
                }
            }
        }else{
            for(int i = 0; i < mF.length; i++){
                for(int  j = i; j < mF.length; j++){
                    suma += (2 * (mF[i][j] * mD[resultado[i]][resultado[j]]));
                    
                }
            }
        }
        return suma;
    }
}