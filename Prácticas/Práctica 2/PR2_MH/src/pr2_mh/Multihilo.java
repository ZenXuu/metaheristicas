/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr2_mh;

import java.util.concurrent.CountDownLatch;

/**
 *
 * @author Jesús
 */
public class Multihilo implements Runnable {

    private StringBuilder log;
    private CountDownLatch cdl;
    private int tam;
    private int[][] flujos;
    private int[][] distancias;
    private long semilla;
    private String nombreAlgoritmo;
    private int k;
    private String nombreArchivo;
    
    public Multihilo(CountDownLatch cdl, long _semilla, int[][] _flujos, int[][] _distancias, int _tam, int _k, String _nombreArchivo) {


        log = new StringBuilder();
        this.cdl = cdl;
        tam = _tam;
        flujos = _flujos;
        distancias = _distancias;
        semilla = _semilla;
        k = _k;
        nombreArchivo = _nombreArchivo;
        

    }

    public Multihilo(int _tam, int[][] _flujos, int[][] _distancias) {
        tam = _tam;
        flujos = _flujos;
        distancias = _distancias;
        log = new StringBuilder();
       
    }

    @Override
    public void run() {
        long tiempoinicial = System.currentTimeMillis();
        long tiempofin;
        
        switch(nombreAlgoritmo){
            case "OX2":
                    Generacional generacional = new Generacional(tam, semilla, distancias, flujos, k, "OX2", log);
                    int costeSolucionGeneracional = generacional.ejecutar();
                    tiempofin = System.currentTimeMillis();
                    double tiempo = (tiempofin - tiempoinicial) / 1000.0000;
                    log.append("\nEl costo final es " + costeSolucionGeneracional + "\nDuracion  " + (tiempofin - tiempoinicial) / 1000.0000 + " segundos");
                    System.out.println("MOC elite " + k + " Semilla: " + semilla +" " + nombreArchivo +"------Coste: " + costeSolucionGeneracional + "   tiempo: " + tiempo);
                    cdl.countDown();
                break;
            case "MOC":
                    Generacional generacional2 = new Generacional(tam, semilla, distancias, flujos, k, "MOC", log);
                    int costeSolucionGeneracional2 = generacional2.ejecutar();
                    tiempofin = System.currentTimeMillis();
                    tiempo = (tiempofin - tiempoinicial) / 1000.0000;
                    log.append("\nEl costo final es " + costeSolucionGeneracional2 + "\nDuracion  " + tiempo + " segundos");
                    System.out.println("MOC elite " + k + " Semilla: " + semilla +" " + nombreArchivo +"------Coste: " + costeSolucionGeneracional2 + "   tiempo: " + tiempo);
                    cdl.countDown();
                break;
            default:
                tiempofin = 0;
                break;
        }


    }

    public void setNombreAlgoritmo(String nombreAlgoritmo) {
        this.nombreAlgoritmo = nombreAlgoritmo;
    }
    
    public String getlog() {
        return log.toString();
    }
}
