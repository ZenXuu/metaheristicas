/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr2_mh;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author migue
 */
public class Configurador {
    private ArrayList<String> archivos;
    private ArrayList<String> cruce;
    private ArrayList<Long> semillas;
    private ArrayList<Integer> k;
    
    public Configurador(String ruta){
        archivos = new ArrayList<>();
        cruce = new ArrayList<>();
        semillas = new ArrayList<>();
        k = new ArrayList<>();
        String linea;
        FileReader f = null;
        try{
            f = new FileReader(ruta);
            BufferedReader b = new BufferedReader(f);
            while((linea = b.readLine())!=null){
                String[] split = linea.split("=");
                switch(split[0]){
                    case "Archivos":
                        String[] v = split[1].split(" ");
                        for(int i=0; i<v.length; i++){
                            archivos.add(v[i]);
                        }
                        break;
                    case "Semillas":
                        String[] vsemillas = split[1].split(" ");
                        for(int i = 0; i < vsemillas.length; i++){
                            semillas.add(Long.parseLong(vsemillas[i]));
                        }
                        break;
                    case "Cruce":
                        String[] valgoritmos = split[1].split(" ");
                        for(int i = 0; i < valgoritmos.length; i++){
                            cruce.add(valgoritmos[i]);
                        }
                        break;
                    case "k-elite":
                        String[] vkelites = split[1].split(" ");
                        for(int i = 0; i < vkelites.length; i++){
                            k.add(Integer.parseInt(vkelites[i]));
                        }
                        break;
                }
            }
        }catch(IOException e){
            System.out.println(e);
        }
    }

    public ArrayList<String> getCruces() {
        return cruce;
    }

    public ArrayList<Integer> getKs() {
        return k;
    }


    public ArrayList<String> getArchivos() {
        return archivos;
    }

    public ArrayList<Long> getSemillas() {
        return semillas;
    }
    
    
}
