/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr3_mh;

import java.util.concurrent.CountDownLatch;

/**
 *
 * @author Jesús
 */
public class Multihilo implements Runnable {

    private StringBuilder log;
    private CountDownLatch cdl;
    private int tam;
    private int[][] flujos;
    private int[][] distancias;
    private long semilla;
    private String nombreAlgoritmo;
    private String nombreArchivo;
    private int iteraciones;
    private int maxGeneraciones;
    private float probMutacion;
    private float probCruce;
    private int tam_pob;
    
    public Multihilo(CountDownLatch cdl, long _semilla, int[][] _flujos, int[][] _distancias, int _tam, String _nombreArchivo, int _iteraciones, int _maxGeneraciones, float _probMutacion, float _probCruce, int _tam_pob) {
        log = new StringBuilder();
        this.cdl = cdl;
        tam = _tam;
        flujos = _flujos;
        distancias = _distancias;
        semilla = _semilla;
        nombreArchivo = _nombreArchivo;
        iteraciones = _iteraciones;
        maxGeneraciones = _maxGeneraciones;
        probMutacion = _probMutacion;
        probCruce = _probCruce;
        tam_pob = _tam_pob;
    }
    
    public Multihilo(int _tam, int[][] _flujos, int[][] _distancias) {
        tam = _tam;
        flujos = _flujos;
        distancias = _distancias;
        log = new StringBuilder();
    }

    @Override
    public void run() {
        long tiempoinicial = System.currentTimeMillis();
        long tiempofin;
        
        switch(nombreAlgoritmo){
            case "OX2":
                    Memestacionario generacional = new Memestacionario(tam, semilla, distancias, flujos, "OX2", log, iteraciones, maxGeneraciones, probMutacion, probCruce, tam_pob);
                    int costeSolucionGeneracional = generacional.ejecutar();
                    tiempofin = System.currentTimeMillis();
                    double tiempo = (tiempofin - tiempoinicial) / 1000.0000;
                    log.append("\nEl costo final es " + costeSolucionGeneracional + "\nDuracion  " + (tiempofin - tiempoinicial) / 1000.0000 + " segundos");
                    //System.out.println("MOC " + iteraciones + "iteraciones Semilla: " + semilla +" " + nombreArchivo +"------Coste: " + costeSolucionGeneracional + "   tiempo: " + tiempo);
                    cdl.countDown();
                break;
            case "MOC":
                    Memestacionario generacional2 = new Memestacionario(tam, semilla, distancias, flujos, "MOC", log, iteraciones, maxGeneraciones, probMutacion, probCruce, tam_pob);
                    int costeSolucionGeneracional2 = generacional2.ejecutar();
                    tiempofin = System.currentTimeMillis();
                    tiempo = (tiempofin - tiempoinicial) / 1000.0000;
                    log.append("\nEl costo final es " + costeSolucionGeneracional2 + "\nDuracion  " + tiempo + " segundos");
                    //System.out.println("MOC " + iteraciones + " iteraciones  Semilla: " + semilla +" " + nombreArchivo +"------Coste: " + costeSolucionGeneracional2 + "   tiempo: " + tiempo);
                    cdl.countDown();
                break;
            default:
                tiempofin = 0;
                break;
        }
    }

    public void setNombreAlgoritmo(String nombreAlgoritmo) {
        this.nombreAlgoritmo = nombreAlgoritmo;
    }
    
    public String getlog() {
        return log.toString();
    }
}
