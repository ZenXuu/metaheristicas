/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr3_mh;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author migue
 */
public class Configurador {
    private ArrayList<String> archivos;
    private ArrayList<String> cruce;
    private ArrayList<Long> semillas;
    private ArrayList<Integer> iteraciones;
    private ArrayList<Integer> generaciones;
    private ArrayList<Float> probmut;
    private ArrayList<Float> probCruce;
    private ArrayList<Integer> tam_pob;
    
    public Configurador(String ruta){
        archivos = new ArrayList<>();
        cruce = new ArrayList<>();
        semillas = new ArrayList<>();
        iteraciones = new ArrayList<>();
        generaciones = new ArrayList<>();
        probmut = new ArrayList<>();
        probCruce = new ArrayList<>();
        tam_pob = new ArrayList<>();
        
        String linea;
        FileReader f = null;
        try{
            f = new FileReader(ruta);
            BufferedReader b = new BufferedReader(f);
            while((linea = b.readLine())!=null){
                String[] split = linea.split("=");
                switch(split[0]){
                    case "Archivos":
                        String[] v = split[1].split(" ");
                        for(int i=0; i<v.length; i++){
                            archivos.add(v[i]);
                        }
                        break;
                    case "Semillas":
                        String[] vsemillas = split[1].split(" ");
                        for(int i = 0; i < vsemillas.length; i++){
                            semillas.add(Long.parseLong(vsemillas[i]));
                        }
                        break;
                    case "Cruce":
                        String[] valgoritmos = split[1].split(" ");
                        for(int i = 0; i < valgoritmos.length; i++){
                            cruce.add(valgoritmos[i]);
                        }
                        break;
                    case "iteraciones-tabu":
                        String[] vkelites = split[1].split(" ");
                        for(int i = 0; i < vkelites.length; i++){
                            iteraciones.add(Integer.parseInt(vkelites[i]));
                        }
                        break;
                    case "generaciones":
                        String[] vgeneraciones = split[1].split(" ");
                        generaciones.add(Integer.parseInt(vgeneraciones[0]));
                        break;
                    case "probabilidad-mutacion":
                        String[] vprobmut = split[1].split(" ");
                        probmut.add(Float.parseFloat(vprobmut[0]));
                        break;
                    case "probabilidad-cruce":
                        String[] vprobcruce = split[1].split(" ");
                        probCruce.add(Float.parseFloat(vprobcruce[0]));
                        break;
                    case "tam-poblacion":
                        String[] vtampob = split[1].split(" ");
                        tam_pob.add(Integer.parseInt(vtampob[0]));
                        break;
                }
            }
        }catch(IOException e){
            System.out.println(e);
        }
    }

    public ArrayList<String> getCruces() {
        return cruce;
    }

    public ArrayList<Integer> getIteraciones(){
        return iteraciones;
    }


    public ArrayList<String> getArchivos() {
        return archivos;
    }

    public ArrayList<Long> getSemillas() {
        return semillas;
    }

    public ArrayList<Integer> getGeneraciones() {
        return generaciones;
    }

    public ArrayList<Float> getProbmut() {
        return probmut;
    }

    public ArrayList<Float> getProbCruce() {
        return probCruce;
    }

    public ArrayList<Integer> getTam_pob() {
        return tam_pob;
    }
    
    
    
}
