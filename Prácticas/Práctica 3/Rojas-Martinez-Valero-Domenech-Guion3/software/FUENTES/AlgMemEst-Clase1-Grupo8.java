package pr3_mh;

import static java.lang.Math.random;
import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;
import java.util.Vector;

/**
 *
 * @author admin
 */
public class Memestacionario {
    private Vector<Agente> poblacion;
    private int tam_poblacion;
    private int tam_sol;
    private Random random;
    long semilla;
    private int generacion;
    private int[][] distancias;
    private int[][] flujos;
    private String cruce;
    private int evaluaciones;
    private StringBuilder log;
    private int iteracionesTabu;
    private int maxGeneraciones;
    private float probabilidadMutacion;
    private float probabilidadCruce;
    private int tam_pob;
    
    public Memestacionario(int _tam, long _semilla, int[][] _distancias, int[][] _flujos, String _cruce, StringBuilder _log, int _iteracionesTabu, int _maxGeneraciones, float _probabilidadMutacion, float _probCruce, int _tam_pob) {
        this.tam_poblacion = _tam_pob;
        this.tam_sol = _tam;
        semilla = _semilla;
        this.random = new Random();
        this.random.setSeed(_semilla);
        this.generacion = 0;
        this.distancias = _distancias;
        iteracionesTabu = _iteracionesTabu;
        this.flujos = _flujos;
        this.cruce = _cruce;
        this.evaluaciones = 0;
        this.log = _log;
        maxGeneraciones =_maxGeneraciones;
        probabilidadMutacion = _probabilidadMutacion;
        probabilidadCruce = _probCruce;
    }
    
    public int ejecutar(){
        //1- Generar poblacion
        this.poblacion = generaPoblacion();
        while(generacion < maxGeneraciones){ 
            log.append("Generacion: " + generacion + "\n");
            generacion++;
            //2- Evaluar poblacion
            evaluarPoblacion(poblacion);
            
            //3- Seleccion
            ArrayList<Agente> seleccionados = seleccionPoblacion(poblacion);
            
            if(generacion == 1 || generacion == 2 || generacion == 3){
                log.append("Individuos seleccionados pre-Recombinacion: \n");
                log.append("Individuo 1:");
                guardaSolucionLog(seleccionados.get(0).getSolucion());
                log.append("Individuo 2:");
                guardaSolucionLog(seleccionados.get(1).getSolucion());
            }
            //4- Recombinacion
            ArrayList<Agente> recombinados = recombinacion(seleccionados);
            
            //5- Evaluar los recombinados
            evaluar(recombinados);
            
            if(generacion == 1 || generacion == 2 || generacion == 3){
                log.append("Individuos seleccionados post-Recombinacion: \n");
                log.append("Individuo 1:");
                guardaSolucionLog(seleccionados.get(0).getSolucion());
                log.append("Individuo 2:");
                guardaSolucionLog(seleccionados.get(1).getSolucion());
            }
            log.append("Coste Individuo 1: " + seleccionados.get(0).getCoste() + "\n");
            log.append("Coste Individuo 2: " + seleccionados.get(1).getCoste() + "\n");
            log.append("-------------------------------------\n");
            //6- Reemplazar poblacion
            reemplazarPoblacion(recombinados);                                                                                                                                
        }
        //System.out.println("MejorCoste: " + mejorIndPoblacion());
        return mejorIndPoblacion();
    }
    
    private void evaluar(ArrayList<Agente> recombinados){
        Agente ind1 = recombinados.get(0);
        Agente ind2 = recombinados.get(1);
        if(!ind1.getEvaluado()){
            ind1.setCoste(evaluacion(ind1.getSolucion(), distancias, flujos));
            ind1.setEvaluado(true);
        }
        if(!ind2.getEvaluado()){
            ind2.setCoste(evaluacion(ind2.getSolucion(), distancias, flujos));
            ind2.setEvaluado(true);
        }
    }
    
    private ArrayList<Agente> recombinacion(ArrayList<Agente> seleccionados){
        ArrayList<Agente> recombinados = new ArrayList<>();
        if(cruce == "MOC"){
            Agente padre1 = seleccionados.get(0);
            Agente padre2 = seleccionados.get(1);
            if(random.nextFloat() <= probabilidadCruce){//parametrizar
                try{
                    ArrayList<int[]> descendientes = MOC(padre1.getSolucion(), padre2.getSolucion());

                    int[] s1 = descendientes.get(0), s2 = descendientes.get(1);
                    for(int j = 0; j < tam_sol; j++){
                        if(random.nextFloat() <= probabilidadMutacion){
                            s1 = tresOpt(descendientes.get(0), j);
                        }
                    }
                    for(int j = 0; j < tam_sol; j++){
                        if(random.nextFloat() <= probabilidadMutacion){
                            s2 = tresOpt(descendientes.get(1), j);
                        }
                    }
                    Agente i1 = new Agente(s1, generacion);
                    Agente i2 = new Agente(s2, generacion);
                    recombinados.add(i1);
                    recombinados.add(i2);
                }catch(Exception e){
                    System.out.println(poblacion.size());
                }
            }
        }
        if(cruce == "OX2"){
            Agente padre1 = seleccionados.get(0);
            Agente padre2 = seleccionados.get(1);
            if(random.nextFloat() <= probabilidadCruce){
                int[] hijo = OX2(padre1.getSolucion(), padre2.getSolucion());
                int[] solHijo = hijo;
                for(int j = 0; j < tam_sol; j++){
                    if(random.nextFloat() <= probabilidadMutacion){
                        solHijo = tresOpt(hijo, j);
                    }
                }
                Agente i1 = new Agente(solHijo, generacion);
                recombinados.add(i1);
                int[] hijo2 = OX2(padre2.getSolucion(), padre1.getSolucion());
                int[] solHijo2 = hijo2;
                for(int j = 0; j < tam_sol; j++){
                    if(random.nextFloat() <= probabilidadMutacion){
                        solHijo2 = tresOpt(hijo, j);
                    }
                }
                Agente i2 = new Agente(solHijo2, generacion);
                recombinados.add(i2);
            }
        }
        //Aplicar busqueda tabú
        if(generacion % 50 == 0){//parametrizar
            Agente i1 = recombinados.get(0);
            Agente i2 = recombinados.get(1);
            MemTabu tabu1 = new MemTabu(tam_sol, flujos, distancias, semilla, log, i1.getSolucion(), iteracionesTabu);
            MemTabu tabu2 = new MemTabu(tam_sol, flujos, distancias, semilla, log, i2.getSolucion(), iteracionesTabu);
            i1.setSolucion(tabu1.ejecutar());
            i2.setSolucion(tabu2.ejecutar());
            recombinados.clear();
            recombinados.add(i1);
            recombinados.add(i2);
        }
        return recombinados;
    }
    
    private void guardaPoblacionLog(Vector<Agente> pob){
        for(int i = 0; i < tam_poblacion; i++){
            log.append("Individuo " + i + ": ");
            Agente ind = pob.get(i);
            for(int j = 0; j < tam_sol; j++){
                log.append(ind.getSolucion()[j] + ", ");
            }
            log.append(" Coste: " + ind.getCoste() + "\n");
        }
    }
    
    private void reemplazarPoblacion(ArrayList<Agente> recombinados){
        ArrayList<Integer> peores = dosPeores();
        int posPeor1 = peores.get(0), posPeor2 = peores.get(1);
        Agente peor1 = poblacion.get(posPeor1);
        Agente peor2 = poblacion.get(posPeor2);
        Agente rec1 = recombinados.get(0);
        Agente rec2 = recombinados.get(1);
       
        poblacion.remove(peor1); poblacion.remove(peor2);
        poblacion.add(rec1); poblacion.add(rec2);
        
    }
    
    private ArrayList<Agente> seleccionPoblacion(Vector<Agente> pobAux){
        ArrayList<Agente> seleccionados = new ArrayList<>();
        for(int i = 0; i <= 1; i++){
            ArrayList<Integer> pos = torneoBinario();
            int pos1 = pos.get(0), pos2 = pos.get(1); 
            if(poblacion.get(pos1).getCoste() < poblacion.get(pos2).getCoste()){
                seleccionados.add(pobAux.get(pos1));
            }else{
                seleccionados.add(pobAux.get(pos2));
            }
        }
        return seleccionados;
    }
    
    /**
     * @brief evalua la poblacion pasada como parametro, dandole valor al coste de cada uno de sus individuos
     * @param pob [in] 
     */
    private void evaluarPoblacion(Vector<Agente> pob){
        for(int i = 0; i < tam_poblacion; i++){
            Agente ind = pob.get(i);
            if(!ind.getEvaluado()){
                ind.setCoste(evaluacion(ind.getSolucion(), distancias, flujos));
                ind.setEvaluado(true);
                evaluaciones++;
            }
        }
    }
    /**
     * @return el mejor individuo de la poblacion
     */
    private int mejorIndPoblacion(){
        int mejorCoste = Integer.MAX_VALUE;
        for(int i = 0; i < tam_poblacion; i++){
            int costeInd = poblacion.get(i).getCoste();
            if( costeInd < mejorCoste){
                mejorCoste = costeInd;
            }
        }
        return mejorCoste;
    }
    /**
     * @brief funcion del torneo binario en la cual se cogen aleatoriamente dos posiciones diferentes de la poblacion
     * @return array con las dos posiciones
     */
    private ArrayList<Integer> torneoBinario(){
        int cont = 0;
        boolean distintos = false;
        int pos1 = random.nextInt(tam_poblacion), pos2;
        do{
            pos2 = random.nextInt(tam_poblacion);
            if(pos2 == pos1){
                while(pos2 == pos1){
                    pos2 = random.nextInt(tam_poblacion);
                }
            }
            Agente i1 = poblacion.get(pos1);
            Agente i2 = poblacion.get(pos2);
            if(!vectorIguales(i1.getSolucion(), i2.getSolucion(), tam_sol)){
                distintos = true;
            }
            cont++;
        }while(cont < tam_poblacion && !distintos);
        
        ArrayList<Integer> pos = new ArrayList<>();
        pos.add(pos1);
        pos.add(pos2);
        return pos;
    }
    
    private boolean vectorIguales(int[] sol1, int[] sol2, int tam){
        for(int i = 0; i < tam; i++){
            if(sol1[i] != sol2[i]){
                return false;
            }
        }
        return true;
    }
    
    /**
     * @brief funcion del operador de cruce MOC
     * @param p1 padre 1
     * @param p2 padre 2
     * @return array de los dos hijos producidos tras el cruce
     */
    private ArrayList<int[]> MOC(int[] p1, int[] p2){
        int[] h1 = new int[tam_sol], h2 = new int[tam_sol];
        ArrayList<int[]> hijos = new ArrayList<>();
        for(int i = 0; i < tam_sol; i++){
            h1[i] = p1[i];
            h2[i] = p2[i];
        }
        int punto = 1 + random.nextInt(tam_sol - 2);
        for(int i = punto; i < tam_sol; i++){
            int aux = p1[i];
            int aux2 = p2[i];
            for(int j = 0; j < tam_sol; j++){
                if(h2[j] == aux){
                    h2[j] = -1;
                }
                if(h1[j] == aux2){
                    h1[j] = -1;
                }
            }
        }
        int pos1 = punto, pos2 = punto;
        for(int i = 0; i < tam_sol; i++){
            if(h1[i] == -1){
                h1[i] = p2[pos2++];
            }
            if(h2[i] == -1){
                h2[i] = p1[pos1++];
            }
        }
        hijos.add(h1);
        hijos.add(h2);
        return hijos;
    }
    /**
     * @brief operador de cruce OX2
     * @param p1 primer padre 
     * @param p2 segundo padre
     * @return devuelve la solucion tras aplicar el cruce
     */
    private int[] OX2(int[] p1, int[] p2){
        ArrayList<Integer> seleccionados = new ArrayList<>();
        for(int i = 0; i < tam_sol; i++){
            float prob = random.nextFloat();
            if(prob < 0.5){
                seleccionados.add(i);
            }
        }
        int[] hijo = new int[tam_sol];
        for(int i = 0; i < tam_sol; i++){
            hijo[i] = p2[i];
        }
        for(int i = 0; i < seleccionados.size(); i++){
            int n = p1[seleccionados.get(i)];
            for(int j = 0; j < tam_sol; j++){
                if(hijo[j] == n){
                    hijo[j] = -1;
                }
            }
        }
        for(int i = 0; i < tam_sol; i++){
            if(hijo[i] == -1){
                hijo[i] = p1[seleccionados.remove(0)];
            }
        }
        return hijo;
    }
    /**
     * @brief operador 3opt
     * @param solucion [in][out] solucion a la cual aplicar el operador
     * @param i posicion inicial de la cual se empieza a hacer el 3opt, se seleccionan otros dos indices aleatoriamente
     * @return solucion tras aplicarle el operador 3opt
     */
    private int[] tresOpt(int[] solucion, int i){
        int nuevaSol[] = new int[tam_sol];
        for(int ii = 0; ii < tam_sol; ii++){
            nuevaSol[ii] = solucion[ii];
        }
        
        int j = random.nextInt(tam_sol);
        if(j == i){
            while(j == i){
                j = random.nextInt(tam_sol);
            }
        }
        int k = random.nextInt(tam_sol);
        if(k == i || k == j){
            while(k == i || k == j){
                k = random.nextInt(tam_sol);
            }
        }
        //System.out.println("i: " + i + " j: " + j + " k: " + k); 
        int aux = solucion[i];
        nuevaSol[i] = solucion[k];
        int aux2 = solucion[j];
        nuevaSol[j] = aux;
        nuevaSol[k] = aux2;
        return nuevaSol;
    }
    /**
     * @brief funcion de seleecion de individuos por torneo aleatorio 
     * @return devuelve el individuo seleccionado
     */
    private Agente seleccion(){
        int pos1 = random.nextInt(tam_sol);
        int pos2 = random.nextInt(tam_sol);
        if(pos2 == pos1){
            while(pos2 == pos1){
                pos2 = random.nextInt(tam_sol);
            }
        }
        Agente ind1 = this.poblacion.get(pos1);
        Agente ind2 = this.poblacion.get(pos2);

        int c1 = ind1.getCoste(), c2 = ind2.getCoste();
        //System.out.println("Pos ind1: " + pos1 + " Coste: " + c1 + " Pos ind2: " + pos2 + " Coste: " + c2);
        if(c1 < c2){
            return ind1;
        }else{
            return ind2;
        }
    }
    /**
     * @brief genera una poblacion de tamanio tam_poblacion
     * @return la poblacion generada
     */
    private Vector<Agente> generaPoblacion(){
        Vector<Agente> pob = new Vector<>();
        for(int i = 0; i < tam_poblacion; i++){
            pob.add(generaIndividuo());
        }
        return pob;
    }
    /**
     * @brief genera de forma aleatoria un individuo
     * @return el individuo generado
     */
    private Agente generaIndividuo(){
        int[] sol = new int[tam_sol];
        ArrayList<Integer> listaPosiblesSol = new ArrayList<Integer>();
        for(int i = 0; i < tam_sol; i++){
            listaPosiblesSol.add(i);
        }
        int tamLista = tam_sol;
        int tamSol = 0;
        while(tamSol != tam_sol){
            int loc = random.nextInt(tamLista);
            if(listaPosiblesSol.contains(loc)){
                sol[tamSol++] = loc;
                listaPosiblesSol.remove((Integer)loc);
            }
        }
        Agente ind = new Agente(sol, generacion);
        return ind;
    }
    /**
     * @brief muestra por pantalla una poblacion
     * @param pob [in] la poblacion que va a ser visualizada
     */
    private void visualizaPoblacion(Vector<Agente> pob){
        for(int i = 0; i < pob.size(); i++){
            print(pob.get(i).getSolucion());
            //System.out.println(" ----> Coste: " + pob.get(i).getCoste() + " Posicion: " + i);
        }
    }
    //muestra por pantalla una solucion
    private void print(int[] sol){
        for(int i = 0; i < tam_sol - 1; i++){
            System.out.print(sol[i] + ", ");
        }
        System.out.print(sol[tam_sol - 1]);
    }
    
    /**
     * @brief funcion que devuelve la posicion de los tres mejores individuos de la poblacion
     * @return la posicion de los tres mejores individuos de la poblacion
     */
    private ArrayList<Agente> tresMejores(){
        int mejorCoste1 = Integer.MAX_VALUE, mejorCoste2 = Integer.MAX_VALUE, mejorCoste3 = Integer.MAX_VALUE;
        int p1 = 0, p2 = 0, p3 = 0;
        for(int i = 0; i < tam_poblacion; i++){
            Agente ind = poblacion.get(i);
            if(ind.getCoste() < mejorCoste1){
                mejorCoste3 = mejorCoste2;
                mejorCoste2 = mejorCoste1;
                p3 = p2;
                p2 = p1;
                
                p1 = i;
                mejorCoste1 = ind.getCoste();
            }else{
                if(ind.getCoste() < mejorCoste2){
                    mejorCoste3 = mejorCoste2;
                    p3 = p2;
                    p2 = i;
                    mejorCoste2 = ind.getCoste();
                }else{
                    if(ind.getCoste() < mejorCoste3){
                        p3 = i;
                        mejorCoste3 = ind.getCoste();
                    }
                }
            }
        }
        ArrayList<Agente> mejores = new ArrayList<>();
        mejores.add(poblacion.get(p1));
        mejores.add(poblacion.get(p2));
        mejores.add(poblacion.get(p3));
        return mejores;
    }
    /**
     * @brief funcion que devuelve la posicion de los tres peores individuos de la poblacion
     * @return la posicion de los tres peores individuos de la poblacion
     */
    private ArrayList<Integer> tresPeores(){
        int mejorCoste1 = Integer.MIN_VALUE, mejorCoste2 = Integer.MIN_VALUE, mejorCoste3 = Integer.MIN_VALUE;
        int p1 = 0, p2 = 0, p3 = 0;
        for(int i = 0; i < tam_poblacion; i++){
            Agente ind = poblacion.get(i);
            if(ind.getCoste() > mejorCoste1){
                mejorCoste3 = mejorCoste2;
                mejorCoste2 = mejorCoste1;
                p3 = p2;
                p2 = p1;
                
                p1 = i;
                mejorCoste1 = ind.getCoste();
            }else{
                if(ind.getCoste() > mejorCoste2){
                    mejorCoste3 = mejorCoste2;
                    p3 = p2;
                    p2 = i;
                    mejorCoste2 = ind.getCoste();
                }else{
                    if(ind.getCoste() > mejorCoste3){
                        p3 = i;
                        mejorCoste3 = ind.getCoste();
                    }
                }
            }
        }
        ArrayList<Integer> peores = new ArrayList<>();
        peores.add(p1);
        peores.add(p2);
        peores.add(p3);
        return peores;
    }
    
    /**
     * @brief funcion que devuelve la posicion de los dos mejores individuos de la poblacion
     * @return la posicion de los dos mejores individuos de la poblacion
     */
    private ArrayList<Agente> dosMejores(){
        int mejorCoste1 = Integer.MAX_VALUE, mejorCoste2 = Integer.MAX_VALUE;
        
        int m1 = 0, m2 = 0;
        for(int i = 0; i < tam_poblacion; i++){
            Agente ind = poblacion.get(i);
            if(ind.getCoste() < mejorCoste1){
                mejorCoste2 = mejorCoste1;
                m2 = m1;
                mejorCoste1 = ind.getCoste();
                m1 = i;
            }else{
                if(ind.getCoste() < mejorCoste2){
                    m2 = i;
                    mejorCoste2 = ind.getCoste();
                }
            }
        }
        
        ArrayList<Agente> mejores = new ArrayList<>();
        mejores.add(poblacion.get(m1));
        mejores.add(poblacion.get(m2));
        
        return mejores;
    }
    /**
     * @brief funcion que devuelve la posicion de los dos peores individuos de la poblacion
     * @return la posicion de los dos peores individuos de la poblacion
     */
    private ArrayList<Integer> dosPeores(){
        int peorCoste1 = Integer.MIN_VALUE, peorCoste2 = Integer.MIN_VALUE;
        ArrayList<Integer> mejores = new ArrayList<>();
        int p1 = 0, p2 = 0;
        for(int i = 0; i < tam_poblacion; i++){
            Agente ind = poblacion.get(i);
            if(ind.getCoste() > peorCoste1){
                peorCoste2 = peorCoste1;
                p2 = p1;
                peorCoste1 = ind.getCoste();
                p1 = i;
            }else{
                if(ind.getCoste() > peorCoste2){
                    p2 = i;
                    peorCoste2 = ind.getCoste();
                }
            }
        }
        mejores.add(p1);
        mejores.add(p2);
        
        return mejores;
    }
     
    private void traspuesta(int mat[][], int tr[][]){
        for (int i = 0; i < tam_sol; i++) 
            for (int j = 0; j < tam_sol; j++) 
                tr[i][j] = mat[j][i]; 
    }
    
    private boolean esSimetrica(int mat[][]){
        int[][] trasp = new int[tam_sol][tam_sol];
        traspuesta(mat, trasp);
        
        for(int i = 0; i < tam_sol; i++){
            for(int j = 0; j < tam_sol; j++){
                if(mat[i][j] != trasp[i][j])
                    return false;
            }
        }
        return true;
    }
    
    public int evaluacion(int[] resultado, int[][] mD, int[][] mF) {
        int suma = 0;
        if(!esSimetrica(mF)){    
            for (int i = 0; i < mF.length; i++) {
                for (int j = 0; j < mF.length; j++) {
                    suma += (mF[i][j] * mD[resultado[i]][resultado[j]]);
                }
            }
        }else{
            for(int i = 0; i < mF.length; i++){
                for(int  j = i; j < mF.length; j++){
                    suma += (2 * (mF[i][j] * mD[resultado[i]][resultado[j]]));
                    
                }
            }
        }
        return suma;
    }
    
    private void guardaSolucionLog(int[] sol){
        for(int i = 0; i < tam_sol - 1; i++){
            log.append(sol[i] + ", ");
        }
        log.append(sol[tam_sol - 1] + "\n");
    }
}



