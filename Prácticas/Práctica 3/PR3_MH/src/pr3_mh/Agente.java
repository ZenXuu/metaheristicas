package pr3_mh;

/**
 *
 * @author admin
 */
public class Agente {
    private int[] solucion;
    private int generacion;
    private int coste;
    private boolean evaluado;
    
    public Agente(int[] solucion, int generacion) {
        this.solucion = solucion;
        this.generacion = generacion;
        this.coste = Integer.MAX_VALUE;
        evaluado = false;
    }

    public void setSolucion(int[] solucion) {
        this.solucion = solucion;
    }
    
    public void setCoste(int _coste){
        this.coste = _coste;
    }
    
    public int[] getSolucion() {
        return solucion;
    }

    public int getCoste() {
        return coste;
    }

    public boolean getEvaluado() {
        return evaluado;
    }

    public void setEvaluado(boolean evaluado) {
        this.evaluado = evaluado;
    }
    
}
