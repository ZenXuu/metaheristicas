/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr3_mh;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author migue
 */
public class leeMatrices {
    private int tam;
    private int[][]flujos;
    private int[][] distancias;

    
    
    public leeMatrices() {
    }
    public void ejecutar(String nombreArchivo){
        try {
            File archivo = new File(nombreArchivo);
            Scanner s = null;
            
            s= new Scanner(archivo);
            
            tam = new Integer(s.nextInt());//Cogemos el tamaÃ±o

            flujos = new int[tam][tam];//Matriz de flujos
            
            for(int i = 0; i < tam; i++){
                for(int j = 0; j < tam; j++){
                    flujos[i][j] = s.nextInt();//leemos cada flujo y lo almacenamos en la matriz
                }
            }
            
            distancias = new int[tam][tam];//Matriz de distancias
            
            for(int i = 0; i < tam; i++){
                for(int j = 0; j < tam; j++){
                    distancias[i][j] = s.nextInt();
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(leeMatrices.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }

    public int getTam() {
        return tam;
    }

    public int[][] getFlujos() {
        return flujos;
    }

    public int[][] getDistancias() {
        return distancias;
    }

}
