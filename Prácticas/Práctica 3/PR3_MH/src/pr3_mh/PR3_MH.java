package pr3_mh;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miguel Ángel Valero, Jesus Rojas GRUPO 8
 */
public class PR3_MH {

    /**
     * @param args the command line arguments
     */
    public static void guardararchivo(String ruta, String texto) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter(ruta);
            pw = new PrintWriter(fichero);
            pw.print(texto);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fichero) {
                    fichero.close();
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }

    }

    public static void leeMatrices(String nombreArchivo, int tam, int[][] flujos, int[][] distancias) {
        try {
            File archivo = new File(nombreArchivo);
            Scanner s = null;

            s = new Scanner(archivo);

            tam = s.nextInt();//Cogemos el tamaÃ±o

            flujos = new int[tam][tam];//Matriz de flujos

            for (int i = 0; i < tam; i++) {
                for (int j = 0; j < tam; j++) {
                    flujos[i][j] = s.nextInt();//leemos cada flujo y lo almacenamos en la matriz
                }
            }

            distancias = new int[tam][tam];//Matriz de distancias

            for (int i = 0; i < tam; i++) {
                for (int j = 0; j < tam; j++) {
                    distancias[i][j] = s.nextInt();
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PR3_MH.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {
        
        Configurador configurador = new Configurador("config.txt");
        ExecutorService ejecutor = Executors.newCachedThreadPool();
        
        for (int i = 0; i < configurador.getCruces().size(); i++) {
            String nombremetaheuristica = configurador.getCruces().get(i);
            for (int j = 0; j < configurador.getArchivos().size(); j++) {
                for(int x = 0; x < configurador.getIteraciones().size(); x++){
                int tam = 0;
                int[][] flujos = null, distancias = null;
                String nombreArchivo = configurador.getArchivos().get(j);
                leeMatrices lM = new leeMatrices();
                lM.ejecutar(nombreArchivo);
                tam = lM.getTam();
                flujos = lM.getFlujos();
                distancias = lM.getDistancias();

                switch (configurador.getCruces().get(i)) {
                    case ("OX2"):
                        ArrayList<Multihilo> m = new ArrayList();
                        CountDownLatch cdl = new CountDownLatch(configurador.getSemillas().size());
                        for (int k = 0; k < configurador.getSemillas().size(); k++) {
                                Multihilo muhilo = new Multihilo(cdl, configurador.getSemillas().get(k), flujos, distancias, tam, nombreArchivo, configurador.getIteraciones().get(x),
                                        configurador.getGeneraciones().get(0), configurador.getProbmut().get(0), configurador.getProbCruce().get(0), configurador.getTam_pob().get(0));
                                muhilo.setNombreAlgoritmo("OX2");
                                m.add(muhilo);
                                ejecutor.execute(muhilo);
                        }
                        cdl.await();
                        for (int k = 0; k < m.size(); k++) {
                            guardararchivo("log/" + configurador.getCruces().get(i) + configurador.getIteraciones().get(x) + "-" + configurador.getSemillas().get(k) + nombreArchivo + ".txt", m.get(k).getlog());
                            //System.out.println("\nFinalizado Algoritmo de Poblacion Generacional realizado al Archivo: "+ configurador.getCruces().get(i) + configurador.getIteraciones().get(x) + "-" + configurador.getSemillas().get(k) + nombreArchivo + " LOG creado.");
                        }

                        break;

                    case ("MOC"):
                        ArrayList<Multihilo> m2 = new ArrayList();
                        CountDownLatch cdl2 = new CountDownLatch(configurador.getSemillas().size());
                        for (int k = 0; k < configurador.getSemillas().size(); k++) {
                                Multihilo muhilo = new Multihilo(cdl2, configurador.getSemillas().get(k), flujos, distancias, tam, nombreArchivo, configurador.getIteraciones().get(x), 
                                        configurador.getGeneraciones().get(0), configurador.getProbmut().get(0), configurador.getProbCruce().get(0), configurador.getTam_pob().get(0));
                                muhilo.setNombreAlgoritmo("MOC");
                                m2.add(muhilo);
                                ejecutor.execute(muhilo);
                        }
                        cdl2.await();
                        for (int k = 0; k < m2.size(); k++) {
                            guardararchivo("log/" + configurador.getCruces().get(i) + configurador.getIteraciones().get(x) + "-" + configurador.getSemillas().get(k) + nombreArchivo + ".txt", m2.get(k).getlog());
                            //System.out.println("\nFinalizado Algoritmo de Poblacion Generacional realizado al Archivo: "+ configurador.getCruces().get(i) + configurador.getIteraciones().get(x) + "-" + configurador.getSemillas().get(k) + nombreArchivo + " LOG creado.");
                        }

                        break;
                }
                
                }
            }

        }
        ejecutor.shutdown();
    }
}
