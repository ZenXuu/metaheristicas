Solucion: 4, 29, 14, 2, 23, 12, 15, 28, 5, 24, 22, 19, 18, 7, 16, 9, 11, 6, 25, 8, 26, 21, 3, 20, 10, 0, 1, 27, 17, 13 
El coste de la solucion inicial es 195613
---------------------
Movimiento: 1 28
Iteracion 0
Coste mejor: 195613 
Se acepta solucion con coste 195007
---------------------
Movimiento: 25 7
Iteracion 1
Coste mejor: 195007 
Se acepta solucion con coste 193824
---------------------
Movimiento: 10 16
Iteracion 2
Coste mejor: 193824 
Se acepta solucion con coste 192636
---------------------
Movimiento: 15 3
Iteracion 3
Coste mejor: 192636 
Se acepta solucion con coste 191517
---------------------
Movimiento: 11 23
Iteracion 4
Coste mejor: 191517 
Se acepta solucion con coste 191281
---------------------
Movimiento: 27 20
Iteracion 5
Coste mejor: 191281 
Se acepta solucion con coste 190915
---------------------
Movimiento: 26 12
Iteracion 6
Coste mejor: 190915 
Se acepta solucion con coste 190667
---------------------
Movimiento: 24 29
Iteracion 7
Coste mejor: 190667 
Se acepta solucion con coste 190648
---------------------
Movimiento: 9 3
Iteracion 8
Coste mejor: 190648 
Se acepta solucion con coste 190280
---------------------
Movimiento: 1 19
Iteracion 9
Coste mejor: 190280 
Se acepta solucion con coste 189262
---------------------
Movimiento: 17 8
Iteracion 10
Coste mejor: 189262 
Se acepta solucion con coste 188976
---------------------
Movimiento: 28 20
Iteracion 11
Coste mejor: 188976 
Se acepta solucion con coste 187378
---------------------
Movimiento: 3 10
Iteracion 12
Coste mejor: 187378 
Se acepta solucion con coste 187368
---------------------
Movimiento: 18 10
Iteracion 13
Coste mejor: 187368 
Se acepta solucion con coste 186823
---------------------
Movimiento: 19 24
Iteracion 14
Coste mejor: 186823 
Se acepta solucion con coste 186118
---------------------
Movimiento: 11 5
Iteracion 15
Coste mejor: 186118 
Se acepta solucion con coste 186087
---------------------
Movimiento: 13 29
Iteracion 16
Coste mejor: 186087 
Se acepta solucion con coste 185834
---------------------
Movimiento: 4 19
Iteracion 17
Coste mejor: 185834 
Se acepta solucion con coste 185118
---------------------
Movimiento: 14 12
Iteracion 18
Coste mejor: 185118 
Se acepta solucion con coste 184486
---------------------
Movimiento: 14 18
Iteracion 19
Coste mejor: 184486 
Se acepta solucion con coste 184263
---------------------
Movimiento: 10 15
Iteracion 20
Coste mejor: 184263 
Se acepta solucion con coste 183899
---------------------
Movimiento: 11 18
Iteracion 21
Coste mejor: 183899 
Se acepta solucion con coste 183805
---------------------
Movimiento: 2 16
Iteracion 22
Coste mejor: 183805 
Se acepta solucion con coste 183259
---------------------
Movimiento: 16 0
Iteracion 23
Coste mejor: 183259 
Se acepta solucion con coste 183024
---------------------
Movimiento: 4 18
Iteracion 24
Coste mejor: 183024 
Se acepta solucion con coste 182924
---------------------
Movimiento: 5 4
Iteracion 25
Coste mejor: 182924 
Se acepta solucion con coste 182469
---------------------
Movimiento: 25 10
Iteracion 26
Coste mejor: 182469 
Se acepta solucion con coste 182404
---------------------
Movimiento: 28 19
Iteracion 27
Coste mejor: 182404 
Se acepta solucion con coste 181980
---------------------
Movimiento: 23 13
Iteracion 28
Coste mejor: 181980 
Se acepta solucion con coste 181522
---------------------
Movimiento: 19 25
Iteracion 29
Coste mejor: 181522 
Se acepta solucion con coste 181055
---------------------
Movimiento: 2 13
Iteracion 30
Coste mejor: 181055 
Se acepta solucion con coste 180563
---------------------
Movimiento: 22 6
Iteracion 31
Coste mejor: 180563 
Se acepta solucion con coste 179560
---------------------
Movimiento: 2 4
Iteracion 32
Coste mejor: 179560 
Se acepta solucion con coste 179307
---------------------
Movimiento: 2 13
Iteracion 33
Coste mejor: 179307 
Se acepta solucion con coste 178294
---------------------
Movimiento: 21 13
Iteracion 34
Coste mejor: 178294 
Se acepta solucion con coste 178009

El costo final es 178009
 Duracion  0.375 segundos