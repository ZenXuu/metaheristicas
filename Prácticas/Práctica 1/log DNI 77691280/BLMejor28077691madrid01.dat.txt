Solucion: 2, 11, 13, 19, 9, 12, 8, 16, 15, 10, 6, 14, 3, 1, 4, 18, 5, 17, 0, 7 
El coste de la solucion inicial es 3526
---------------------
Movimiento: 0 3
Iteracion 0
Coste mejor: 3526 
Se acepta solucion con coste 3418
---------------------
Movimiento: 19 12
Iteracion 1
Coste mejor: 3418 
Se acepta solucion con coste 3352
---------------------
Movimiento: 2 12
Iteracion 2
Coste mejor: 3352 
Se acepta solucion con coste 3328
---------------------
Movimiento: 18 15
Iteracion 3
Coste mejor: 3328 
Se acepta solucion con coste 3262
---------------------
Movimiento: 19 4
Iteracion 4
Coste mejor: 3262 
Se acepta solucion con coste 3172
---------------------
Movimiento: 14 17
Iteracion 5
Coste mejor: 3172 
Se acepta solucion con coste 3078
---------------------
Movimiento: 0 5
Iteracion 6
Coste mejor: 3078 
Se acepta solucion con coste 3068
---------------------
Movimiento: 3 18
Iteracion 7
Coste mejor: 3068 
Se acepta solucion con coste 3030
---------------------
Movimiento: 1 7
Iteracion 8
Coste mejor: 3030 
Se acepta solucion con coste 2978
---------------------
Movimiento: 6 5
Iteracion 9
Coste mejor: 2978 
Se acepta solucion con coste 2940
---------------------
Movimiento: 9 3
Iteracion 10
Coste mejor: 2940 
Se acepta solucion con coste 2912
---------------------
Movimiento: 1 2
Iteracion 11
Coste mejor: 2912 
Se acepta solucion con coste 2910
---------------------
Movimiento: 17 15
Iteracion 12
Coste mejor: 2910 
Se acepta solucion con coste 2898
---------------------
Movimiento: 2 10
Iteracion 13
Coste mejor: 2898 
Se acepta solucion con coste 2890
---------------------
Movimiento: 10 15
Iteracion 14
Coste mejor: 2890 
Se acepta solucion con coste 2878
---------------------
Movimiento: 9 12
Iteracion 15
Coste mejor: 2878 
Se acepta solucion con coste 2850
---------------------
Movimiento: 15 16
Iteracion 16
Coste mejor: 2850 
Se acepta solucion con coste 2836
---------------------
Movimiento: 8 9
Iteracion 17
Coste mejor: 2836 
Se acepta solucion con coste 2816
---------------------
Movimiento: 9 4
Iteracion 18
Coste mejor: 2816 
Se acepta solucion con coste 2802
---------------------
Movimiento: 2 16
Iteracion 19
Coste mejor: 2802 
Se acepta solucion con coste 2790
---------------------
Movimiento: 9 2
Iteracion 20
Coste mejor: 2790 
Se acepta solucion con coste 2770
---------------------
Movimiento: 13 12
Iteracion 21
Coste mejor: 2770 
Se acepta solucion con coste 2764
---------------------
Movimiento: 0 5
Iteracion 22
Coste mejor: 2764 
Se acepta solucion con coste 2752
---------------------
Movimiento: 9 0
Iteracion 23
Coste mejor: 2752 
Se acepta solucion con coste 2694
---------------------
Movimiento: 12 13
Iteracion 24
Coste mejor: 2694 
Se acepta solucion con coste 2684
---------------------
Movimiento: 18 19
Iteracion 25
Coste mejor: 2684 
Se acepta solucion con coste 2646

El costo final es 2646
 Duracion  0.5 segundos