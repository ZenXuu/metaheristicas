Solucion: 10, 11, 17, 8, 16, 1, 12, 18, 9, 2, 6, 5, 13, 7, 3, 15, 19, 4, 14, 0, 20 
El coste de la solucion inicial es 3502
---------------------
Movimiento: 13 17
Iteracion 0
Coste mejor: 3502 
Se acepta solucion con coste 3242
---------------------
Movimiento: 12 8
Iteracion 1
Coste mejor: 3242 
Se acepta solucion con coste 3094
---------------------
Movimiento: 1 12
Iteracion 2
Coste mejor: 3094 
Se acepta solucion con coste 2970
---------------------
Movimiento: 3 1
Iteracion 3
Coste mejor: 2970 
Se acepta solucion con coste 2950
---------------------
Movimiento: 15 8
Iteracion 4
Coste mejor: 2950 
Se acepta solucion con coste 2928
---------------------
Movimiento: 11 4
Iteracion 5
Coste mejor: 2928 
Se acepta solucion con coste 2914
---------------------
Movimiento: 11 18
Iteracion 6
Coste mejor: 2914 
Se acepta solucion con coste 2910
---------------------
Movimiento: 2 20
Iteracion 7
Coste mejor: 2910 
Se acepta solucion con coste 2882
---------------------
Movimiento: 13 4
Iteracion 8
Coste mejor: 2882 
Se acepta solucion con coste 2850
---------------------
Movimiento: 20 12
Iteracion 9
Coste mejor: 2850 
Se acepta solucion con coste 2834
---------------------
Movimiento: 0 7
Iteracion 10
Coste mejor: 2834 
Se acepta solucion con coste 2828
---------------------
Movimiento: 0 11
Iteracion 11
Coste mejor: 2828 
Se acepta solucion con coste 2738
---------------------
Movimiento: 14 0
Iteracion 12
Coste mejor: 2738 
Se acepta solucion con coste 2698
---------------------
Movimiento: 6 4
Iteracion 13
Coste mejor: 2698 
Se acepta solucion con coste 2634

El costo final es 2634
 Duracion  0.609 segundos