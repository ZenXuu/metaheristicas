Solucion: 2, 21, 3, 9, 19, 12, 18, 16, 5, 6, 0, 23, 26, 22, 14, 4, 28, 25, 11, 10, 7, 17, 27, 24, 13, 20, 29, 8, 15, 1 
El coste de la solucion inicial es 197116
---------------------
Movimiento: 5 3
Iteracion 0
Coste mejor: 197116 
Se acepta solucion con coste 195201
---------------------
Movimiento: 23 3
Iteracion 1
Coste mejor: 195201 
Se acepta solucion con coste 195048
---------------------
Movimiento: 14 16
Iteracion 2
Coste mejor: 195048 
Se acepta solucion con coste 193668
---------------------
Movimiento: 29 28
Iteracion 3
Coste mejor: 193668 
Se acepta solucion con coste 192209
---------------------
Movimiento: 13 21
Iteracion 4
Coste mejor: 192209 
Se acepta solucion con coste 191756
---------------------
Movimiento: 8 26
Iteracion 5
Coste mejor: 191756 
Se acepta solucion con coste 189917
---------------------
Movimiento: 13 28
Iteracion 6
Coste mejor: 189917 
Se acepta solucion con coste 189223
---------------------
Movimiento: 1 27
Iteracion 7
Coste mejor: 189223 
Se acepta solucion con coste 188399
---------------------
Movimiento: 22 7
Iteracion 8
Coste mejor: 188399 
Se acepta solucion con coste 187748
---------------------
Movimiento: 11 24
Iteracion 9
Coste mejor: 187748 
Se acepta solucion con coste 187124
---------------------
Movimiento: 14 3
Iteracion 10
Coste mejor: 187124 
Se acepta solucion con coste 186807
---------------------
Movimiento: 17 21
Iteracion 11
Coste mejor: 186807 
Se acepta solucion con coste 185855
---------------------
Movimiento: 23 27
Iteracion 12
Coste mejor: 185855 
Se acepta solucion con coste 185371
---------------------
Movimiento: 7 10
Iteracion 13
Coste mejor: 185371 
Se acepta solucion con coste 185064
---------------------
Movimiento: 6 3
Iteracion 14
Coste mejor: 185064 
Se acepta solucion con coste 184470
---------------------
Movimiento: 3 28
Iteracion 15
Coste mejor: 184470 
Se acepta solucion con coste 184161
---------------------
Movimiento: 5 6
Iteracion 16
Coste mejor: 184161 
Se acepta solucion con coste 183464
---------------------
Movimiento: 24 5
Iteracion 17
Coste mejor: 183464 
Se acepta solucion con coste 183384
---------------------
Movimiento: 26 27
Iteracion 18
Coste mejor: 183384 
Se acepta solucion con coste 183039
---------------------
Movimiento: 29 20
Iteracion 19
Coste mejor: 183039 
Se acepta solucion con coste 181435
---------------------
Movimiento: 0 10
Iteracion 20
Coste mejor: 181435 
Se acepta solucion con coste 180188
---------------------
Movimiento: 4 22
Iteracion 21
Coste mejor: 180188 
Se acepta solucion con coste 179704
---------------------
Movimiento: 24 22
Iteracion 22
Coste mejor: 179704 
Se acepta solucion con coste 179652
---------------------
Movimiento: 29 0
Iteracion 23
Coste mejor: 179652 
Se acepta solucion con coste 179483
---------------------
Movimiento: 24 11
Iteracion 24
Coste mejor: 179483 
Se acepta solucion con coste 179242
---------------------
Movimiento: 23 29
Iteracion 25
Coste mejor: 179242 
Se acepta solucion con coste 179174
---------------------
Movimiento: 11 2
Iteracion 26
Coste mejor: 179174 
Se acepta solucion con coste 178963
---------------------
Movimiento: 3 23
Iteracion 27
Coste mejor: 178963 
Se acepta solucion con coste 178892
---------------------
Movimiento: 24 2
Iteracion 28
Coste mejor: 178892 
Se acepta solucion con coste 178498
---------------------
Movimiento: 10 1
Iteracion 29
Coste mejor: 178498 
Se acepta solucion con coste 177923
---------------------
Movimiento: 20 6
Iteracion 30
Coste mejor: 177923 
Se acepta solucion con coste 177753
---------------------
Movimiento: 22 25
Iteracion 31
Coste mejor: 177753 
Se acepta solucion con coste 177742
---------------------
Movimiento: 8 6
Iteracion 32
Coste mejor: 177742 
Se acepta solucion con coste 177698
---------------------
Movimiento: 8 9
Iteracion 33
Coste mejor: 177698 
Se acepta solucion con coste 176916
---------------------
Movimiento: 25 1
Iteracion 34
Coste mejor: 176916 
Se acepta solucion con coste 176269
---------------------
Movimiento: 23 10
Iteracion 35
Coste mejor: 176269 
Se acepta solucion con coste 176068
---------------------
Movimiento: 2 24
Iteracion 36
Coste mejor: 176068 
Se acepta solucion con coste 175987
---------------------
Movimiento: 20 6
Iteracion 37
Coste mejor: 175987 
Se acepta solucion con coste 175671

El costo final es 175671
 Duracion  0.328 segundos