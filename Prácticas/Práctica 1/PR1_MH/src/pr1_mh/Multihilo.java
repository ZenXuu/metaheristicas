/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr1_mh;

import java.util.concurrent.CountDownLatch;

/**
 *
 * @author Jesús
 */
public class Multihilo implements Runnable {

    private StringBuilder log;
    private CountDownLatch cdl;
    private int tam;
    private int[][] flujos;
    private int[][] distancias;
    private long semilla;
    private String nombreAlgoritmo;
    
    public Multihilo(CountDownLatch cdl, long _semilla, int[][] _flujos, int[][] _distancias, int _tam) {


        log = new StringBuilder();
        this.cdl = cdl;
        tam = _tam;
        flujos = _flujos;
        distancias = _distancias;
        semilla = _semilla;

    }

    public Multihilo(int _tam, int[][] _flujos, int[][] _distancias) {
        tam = _tam;
        flujos = _flujos;
        distancias = _distancias;
        log = new StringBuilder();
       
    }

    @Override
    public void run() {
        long tiempoinicial = System.currentTimeMillis();
        long tiempofin;
        
        switch(nombreAlgoritmo){
            case "Greedy":
                    Greedy greedy = new Greedy(tam, flujos, distancias, log);
                    int CosteSolucionG = greedy.ejecutar();
                    tiempofin = System.currentTimeMillis();
                    log.append("\nDuracion  " + (tiempofin - tiempoinicial) / 1000.0000 + " segundos");
                    
                break;
            case "BLMejor":
                    BLMejor blmejor = new BLMejor(tam, flujos, distancias, semilla, log);
                    int costeSolucion = blmejor.ejecutar();
                    tiempofin = System.currentTimeMillis();
                    log.append("\nEl costo final es " + costeSolucion + "\n Duracion  " + (tiempofin - tiempoinicial) / 1000.0000 + " segundos");
                    cdl.countDown();
                    
                break;
            case "Tabu":
                    Tabu tabu = new Tabu(tam, flujos, distancias, semilla, log);
                    int costeSolucionTabu = tabu.ejecutar();
                    tiempofin = System.currentTimeMillis();
                    log.append("\nEl costo final es " + costeSolucionTabu + "\nDuracion  " + (tiempofin - tiempoinicial) / 1000.0000 + " segundos");
                    cdl.countDown();
                break;
            default:
                tiempofin = 0;
                break;
        }
       
        
        
        

    }

    public void setNombreAlgoritmo(String nombreAlgoritmo) {
        this.nombreAlgoritmo = nombreAlgoritmo;
    }
    
    public String getlog() {
        return log.toString();
    }
}
