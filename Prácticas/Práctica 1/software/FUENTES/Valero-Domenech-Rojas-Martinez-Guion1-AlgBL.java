/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr1_mh;

import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;
import javafx.util.Pair;

/**
 *
 * @author migue
 */
public class BLMejor {

    private int tam;
    private int numVecinosEntorno;
    private int[][] flujos;
    private int[][] distancias;
    private long semilla;
    private Random random;
    private StringBuilder log;

    public BLMejor(int _tam, int[][] _flujos, int[][] _distancias, long _semilla, StringBuilder _log) {
        tam = _tam;
        numVecinosEntorno = 10;
        flujos = _flujos;
        distancias = _distancias;
        semilla = _semilla;
        random = new Random();
        random.setSeed(semilla);
        log = _log;
    }

    public int ejecutar() {
        int maxIter = 50000, iter = 0, intentos = 0, maxIntentos = 100;

        int[] mejorSolucion = new int[tam];
        mejorSolucion = generaSolucionInicial();
        int mejorCoste = evaluacion(mejorSolucion, distancias, flujos);
        int mejorR = 0, mejorS = 0;

        log.append("Solucion: ");

        for (int i = 0; i < tam - 1; i++) {
            log.append(mejorSolucion[i] + ", ");
        }
        log.append(mejorSolucion[tam - 1] + " \n");

        log.append("El coste de la solucion inicial es " + mejorCoste + "\n");
        while (iter < maxIter && intentos < maxIntentos) {
            Vector<parIJ> intercambios = new Vector<>();
            intercambios = generaEntorno();
            System.out.println();

            int mejorCosteVecino = Integer.MAX_VALUE;
            for (int i = 0; i < intercambios.size(); i++) {
                int r = intercambios.get(i).i;
                int s = intercambios.get(i).j;

                int costeVecino = Factorizacion(mejorSolucion, distancias, flujos,tam, mejorCoste, r, s);
                //int costeVecino = evaluacion(intercambia(r,s,mejorSolucion), distancias, flujos);
                if (costeVecino < mejorCosteVecino) {
                    mejorCosteVecino = costeVecino;
                    mejorR = r;
                    mejorS = s;
                }
            }

            if (mejorCosteVecino < mejorCoste) {
                log.append("---------------------");
                log.append("\nMovimiento: " + mejorR + " " + mejorS + "\nIteracion " + iter + "\nCoste mejor: " + mejorCoste + " \nSe acepta solucion con coste " + mejorCosteVecino + "\n");
                mejorCoste = mejorCosteVecino;
                mejorSolucion = intercambia(mejorR, mejorS, mejorSolucion);
                intentos = 0;
                iter++;
            } else {
                intentos++;
            }
        }
        return mejorCoste;
    }

    int[] generaSolucionInicial() {
        int[] sol = new int[tam];
        ArrayList<Integer> listaPosiblesSol = new ArrayList<Integer>();
        for (int i = 0; i < tam; i++) {
            listaPosiblesSol.add(i);
        }
        int tamLista = tam;
        int tamSol = 0;
        while (tamSol != tam) {
            int loc = random.nextInt(tamLista);
            if (listaPosiblesSol.contains(loc)) {
                sol[tamSol++] = loc;
                listaPosiblesSol.remove((Integer) loc);
            }
        }
        return sol;
    }

    private void traspuesta(int mat[][], int tr[][]) {
        for (int i = 0; i < tam; i++) {
            for (int j = 0; j < tam; j++) {
                tr[i][j] = mat[j][i];
            }
        }
    }

    private boolean esSimetrica(int mat[][]) {
        int[][] trasp = new int[tam][tam];
        traspuesta(mat, trasp);

        for (int i = 0; i < tam; i++) {
            for (int j = 0; j < tam; j++) {
                if (mat[i][j] != trasp[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public int evaluacion(int[] resultado, int[][] mD, int[][] mF) {
        int suma = 0;
        if (!esSimetrica(mF)) {
            for (int i = 0; i < mF.length; i++) {
                for (int j = 0; j < mF.length; j++) {
                    suma += (mF[i][j] * mD[resultado[i]][resultado[j]]);
                }
            }
        } else {
            for (int i = 0; i < mF.length; i++) {
                for (int j = i; j < mF.length; j++) {
                    suma += (2 * (mF[i][j] * mD[resultado[i]][resultado[j]]));

                }
            }
        }
        return suma;
    }

    public int Factorizacion(int[] mejorSolucion, int[][] dist, int[][] fluj, int tam, int mejorCoste, int r, int s) {
        for (int k = 0; k < tam; k++) {
            if (k != r && k != s) {
                mejorCoste += fluj[r][k] * (dist[mejorSolucion[s]][mejorSolucion[k]] - dist[mejorSolucion[r]][mejorSolucion[k]])
                            + fluj[s][k] * (dist[mejorSolucion[r]][mejorSolucion[k]] - dist[mejorSolucion[s]][mejorSolucion[k]])
                            + fluj[k][r] * (dist[mejorSolucion[k]][mejorSolucion[s]] - dist[mejorSolucion[k]][mejorSolucion[r]])
                            + fluj[k][s] * (dist[mejorSolucion[k]][mejorSolucion[r]] - dist[mejorSolucion[k]][mejorSolucion[s]]);
            }
        }
        return mejorCoste;
    }

    /**
     * @brief funcion del operador 2opt, intercambia dos valores en una solucion
     * @param vector la solucion dada
     * @param i posicion del valor a intercambiar con j
     * @param j posicion del valor a intercambiar con i
     */
    private int[] intercambia(int r, int s, int[] vector) {
        int[] vAux = new int[tam];
        for (int i = 0; i < tam; i++) {
            vAux[i] = vector[i];
        }
        int aux = vector[r];
        vAux[r] = vector[s];
        vAux[s] = aux;
        return vAux;
    }

    private Vector<parIJ> generaEntorno() {
        Vector<parIJ> vectorIJ = new Vector<parIJ>();
        int i = random.nextInt(tam);
        int j = random.nextInt(tam);
        parIJ par = new parIJ(i, j);
        vectorIJ.add(par);
        int tamVecinosGenerados = 1;
        while (tamVecinosGenerados < 10) {
            i = random.nextInt(tam);
            j = random.nextInt(tam);
            if (!ijRepetido(i, j, vectorIJ)) {
                parIJ p = new parIJ(i, j);
                vectorIJ.add(p);
                tamVecinosGenerados++;
            }
        }
        return vectorIJ;
    }

    private boolean ijRepetido(int i, int j, Vector<parIJ> vectorIJ) {
        for (int k = 0; k < vectorIJ.size(); k++) {
            parIJ par = vectorIJ.get(k);
            if ((par.getI() == i && par.getJ() == j) || (par.getI() == j && par.getJ() == i)) {
                return true;
            }
        }
        return false;
    }

    class parIJ {

        private int i;
        private int j;

        public parIJ(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final parIJ other = (parIJ) obj;
            if (this.i != other.i) {
                return false;
            }
            if (this.j != other.j) {
                return false;
            }
            return true;
        }
    }

    private void print(int tam, int[] v) {
        for (int i = 0; i < tam; i++) {
            System.out.print(v[i] + " ");
        }
        System.out.println();
    }

}
