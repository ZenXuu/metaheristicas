/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr1_mh;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.stream.DoubleStream;

/**
 *
 * @author migue
 */
public class Tabu {

    private int tenencia;
    private int tam;
    private int numVecinosEntorno;
    private int[][] flujos;
    private int[][] distancias;
    private long semilla;
    private Random random;
    private StringBuilder log;
    private ArrayList<int[]> listaTabu;
    private ArrayList<parIJ> listaMov;
    private int[][] matrizFrec;

    public Tabu(int _tam, int[][] _flujos, int[][] _distancias, long _semilla, StringBuilder _log) {
        tam = _tam;
        tenencia = 5;
        numVecinosEntorno = 10;
        flujos = _flujos;
        distancias = _distancias;
        semilla = _semilla;
        random = new Random();
        random.setSeed(semilla);
        //random = new Aleatorio();
        //random.Set_random(semilla);
        listaTabu = new ArrayList<>();
        listaMov = new ArrayList<>();
        log = _log;
        matrizFrec = new int[tam][tam];
        for (int i = 0; i < tam; i++) {
            for (int j = 0; j < tam; j++) {
                matrizFrec[i][j] = 0;
            }
        }
    }

    public int ejecutar() {
        int maxIntentos = 100, intentos = 0, estancamientos = 0, maxEstancamientos = 100;
        int maxIteraciones = 50000, iter = 0;
        int[] solucionActual = new int[tam];

        solucionActual = generaSolucionInicial();//generamos una solucion inicial de forma aleatoria
        int[] mejorSolucion = solucionActual;
        int[] mejorDePeores = null;
        int costeActual = evaluacion(solucionActual, distancias, flujos);
        int mejorCoste = costeActual;
        int costeMejorDePeores = Integer.MAX_VALUE;

        log.append("Solucion: ");

        for (int i = 0; i < tam - 1; i++) {
            log.append(mejorSolucion[i] + ", ");
        }
        log.append(mejorSolucion[tam - 1] + " \n");

        log.append("El coste de la solucion inicial es " + mejorCoste + "\n");

        while (iter < maxIteraciones) {
            //Generamos 10 posibles cambios de posicion que seria el entorno
            Vector<parIJ> posiblesIntercambios = generaEntorno();

            int costeMejorVecino = Integer.MAX_VALUE;//coste de mejor vecino lo iniciamos a infinito
            //esto son los indices que se cambian de la solucionActual para conseguir el mejor vecino:
            int mejorR = -1, mejorS = -1;

            for (int i = 0; i < 10; i++) {
                int r = posiblesIntercambios.get(i).getI();
                int s = posiblesIntercambios.get(i).getJ();

                int aux = Factorizacion(solucionActual, distancias, flujos, tam, costeActual, r, s);
                //int aux = evaluacion(intercambia(r, s, solucionActual), distancias, flujos);
                //si el coste del vecino es mejor que el del mejorVecino actual, lo actualizamos
                if (aux < costeMejorVecino
                        && !esTabuSol(intercambia(r, s, solucionActual))
                        && !esTabuMov(r, s)) {
                    costeMejorVecino = aux;
                    mejorR = r;
                    mejorS = s;
                }
            }

            if (costeMejorVecino < costeActual) {//hay un vecino en el entorno que mejora nuestra solucion
                //log
                log.append("---------------------");
                log.append("\nMovimiento: " + mejorR + " " + mejorS + "\nIteracion " + iter + "\nCoste mejor: " + mejorCoste + "\nCoste Actual: " + costeActual + " \nSe acepta solucion con coste " + costeMejorVecino + "\n");

                solucionActual = intercambia(mejorR, mejorS, solucionActual);
                costeActual = costeMejorVecino;

                if (costeActual < mejorCoste) {
                    mejorCoste = costeActual;
                    mejorSolucion = solucionActual;
                    estancamientos = 0;
                }

                intentos = 0;
                iter++;

                //añadimos a lista tabu(solucion), lista mov(valores de la solucion en los indices)
                if (listaTabu.size() < tenencia) {
                    listaTabu.add(solucionActual);
                } else {
                    listaTabu.remove(0);
                    listaTabu.add(solucionActual);
                }
                parIJ par = new parIJ(mejorR, mejorS);
                if (listaMov.size() < tenencia) {
                    listaMov.add(par);
                } else {
                    listaMov.remove(0);
                    listaMov.add(par);
                }
                //actualizamos matriz de frecuencias
                //matrizFrec[mejorR][solucionActual[mejorS]]++;
                for (int i = 0; i < tam; i++) {
                    matrizFrec[i][solucionActual[i]]++;
                }
            } else {
                intentos++;//aumentamos los intentos puesto que tras generar el entorno no nos movemos
                //Si el coste del mejor vecino es menor que el coste del mejor de peores, actualizamos este ultimo
                if (costeMejorVecino < costeMejorDePeores) {
                    costeMejorDePeores = costeMejorVecino;
                    mejorDePeores = intercambia(mejorR, mejorS, solucionActual);
                }
            }

            if (intentos == maxIntentos) {//si llegamos a 100 intentos de movernos
                log.append("------------100 INTENTOS -> Nos movemos al mejor de peores------------");
                log.append("\nIteracion " + iter + "\nCoste mejor de peores: " + costeMejorDePeores + "\n");
                solucionActual = mejorDePeores;
                costeActual = costeMejorDePeores;
                costeMejorDePeores = Integer.MAX_VALUE;
                estancamientos++;//aumentamos estancamientos porque nos movemos al mejor de peores
                iter++;//aumentamos iteraciones porque nos movemos
                intentos = 0;//reinicilizamos intentos porque nos movemos
            }
            if (estancamientos == maxEstancamientos) {//si hemos llegado a los 100 estancamientos
                log.append("------------100 ESTANCAMIENTOS -> OSCILACION ESTRATEGICA------------\n");
                //Oscilacion estrategica
                float prob = random.nextFloat();
                if (prob > 0.5) {
                    //Diversificamos
                    log.append("------------DIVERSIFICAMOS------------");
                    //print(tam, solucionActual);
                    solucionActual = solDiversificar();
                    //print(tam, solucionActual);
                } else {
                    //Intensificamos
                    log.append("------------INTENSIFICAMOS------------");
                    //print(tam, solucionActual);
                    solucionActual = solIntensificar();
                    //print(tam, solucionActual);
                }
                costeActual = evaluacion(solucionActual, distancias, flujos);
                log.append("\nIteracion " + iter + "\nCoste mejor: " + mejorCoste + "\nCoste nueva solucion: " + costeActual + "\n");
                costeMejorDePeores = Integer.MAX_VALUE;
                //reinicializar las memorias
                listaTabu.clear();
                listaMov.clear();
                matrizFrec = matrizVacia();
                estancamientos = 0;
                iter++;
                intentos = 0;
            }
        }
        return mejorCoste;

    }

    int[] solDiversificar() {
        int[] sol = new int[tam];
        int min = Integer.MAX_VALUE;
        int auxJ = 0;
        HashSet<Integer> set = new HashSet<>();
        for (int i = 0; i < tam; i++) {
            min = Integer.MAX_VALUE;
            for (int j = 0; j < tam; j++) {
                if (matrizFrec[i][j] < min && !set.contains(j)) {
                    min = matrizFrec[i][j];
                    auxJ = j;
                }
            }
            set.add(auxJ);
            sol[i] = auxJ;
            matrizFrec[i][auxJ] = Integer.MAX_VALUE;
        }
        return sol;
    }

    int[] solIntensificar() {
        int[] sol = new int[tam];
        int max = Integer.MIN_VALUE;
        int auxJ = 0;
        HashSet<Integer> set = new HashSet<>();
        for (int i = 0; i < tam; i++) {
            max = Integer.MIN_VALUE;
            for (int j = 0; j < tam; j++) {
                if (matrizFrec[i][j] > max && !set.contains(j)) {
                    max = matrizFrec[i][j];
                    auxJ = j;
                }
            }
            set.add(auxJ);
            sol[i] = auxJ;
            matrizFrec[i][auxJ] = -1;
        }
        return sol;
    }

    private boolean esTabuMov(int valorR, int valorS) {
        for (int i = 0; i < listaMov.size(); i++) {
            parIJ par = listaMov.get(i);
            //if((par.i == valorR && par.j == valorS) || (par.i == valorS && par.j == valorR)){
            if (par.i == valorR && par.j == valorS) {
                return true;
            }
        }
        return false;
    }

    private boolean esTabuSol(int[] solucion) {
        for (int i = 0; i < listaTabu.size(); i++) {
            int[] solAux = listaTabu.get(i);
            int cont = 0;
            for (int j = 0; j < tam; j++) {
                if (solucion[j] == solAux[j]) {
                    cont++;
                }
            }
            if (cont == tam - 1) {
                return true;
            }
        }
        return false;
    }

    int[][] matrizVacia() {
        int mat[][] = new int[tam][tam];
        for (int i = 0; i < tam; i++) {
            for (int j = 0; j < tam; j++) {
                mat[i][j] = 0;
            }
        }
        return mat;
    }

    int[] generaSolucionInicial() {
        int[] sol = new int[tam];
        ArrayList<Integer> listaPosiblesSol = new ArrayList<Integer>();
        for (int i = 0; i < tam; i++) {
            listaPosiblesSol.add(i);
        }
        int tamLista = tam;
        int tamSol = 0;
        while (tamSol != tam) {
            int loc = random.nextInt(tamLista);
            if (listaPosiblesSol.contains(loc)) {
                sol[tamSol++] = loc;
                listaPosiblesSol.remove((Integer) loc);
            }
        }
        return sol;
    }

    private void traspuesta(int mat[][], int tr[][]) {
        for (int i = 0; i < tam; i++) {
            for (int j = 0; j < tam; j++) {
                tr[i][j] = mat[j][i];
            }
        }
    }

    private boolean esSimetrica(int mat[][]) {
        int[][] trasp = new int[tam][tam];
        traspuesta(mat, trasp);

        for (int i = 0; i < tam; i++) {
            for (int j = 0; j < tam; j++) {
                if (mat[i][j] != trasp[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public int evaluacion(int[] resultado, int[][] mD, int[][] mF) {
        int suma = 0;
        if (!esSimetrica(mF)) {
            for (int i = 0; i < mF.length; i++) {
                for (int j = 0; j < mF.length; j++) {
                    suma += (mF[i][j] * mD[resultado[i]][resultado[j]]);
                }
            }
        } else {
            for (int i = 0; i < mF.length; i++) {
                for (int j = i; j < mF.length; j++) {
                    suma += (2 * (mF[i][j] * mD[resultado[i]][resultado[j]]));

                }
            }
        }
        return suma;
    }

    public int Factorizacion(int[] mejorSolucion, int[][] dist, int[][] fluj, int tam, int mejorCoste, int r, int s) {
        for (int k = 0; k < tam; k++) {
            if (k != r && k != s) {
                mejorCoste += fluj[r][k] * (dist[mejorSolucion[s]][mejorSolucion[k]] - dist[mejorSolucion[r]][mejorSolucion[k]])
                            + fluj[s][k] * (dist[mejorSolucion[r]][mejorSolucion[k]] - dist[mejorSolucion[s]][mejorSolucion[k]])
                            + fluj[k][r] * (dist[mejorSolucion[k]][mejorSolucion[s]] - dist[mejorSolucion[k]][mejorSolucion[r]])
                            + fluj[k][s] * (dist[mejorSolucion[k]][mejorSolucion[r]] - dist[mejorSolucion[k]][mejorSolucion[s]]);
            }
        }
        return mejorCoste;
    }

    /**
     * @brief funcion del operador 2opt, intercambia dos valores en una solucion
     * @param vector la solucion dada
     * @param i posicion del valor a intercambiar con j
     * @param j posicion del valor a intercambiar con i
     */
    private int[] intercambia(int r, int s, int[] vector) {
        int[] vAux = new int[tam];
        for (int i = 0; i < tam; i++) {
            vAux[i] = vector[i];
        }
        int aux = vector[r];
        vAux[r] = vector[s];
        vAux[s] = aux;
        return vAux;
    }

    private Vector<parIJ> generaEntorno() {
        Vector<parIJ> vectorIJ = new Vector<parIJ>();
        int i = random.nextInt(tam);
        int j = random.nextInt(tam);
        parIJ par = new parIJ(i, j);
        vectorIJ.add(par);
        int tamVecinosGenerados = 1;
        while (tamVecinosGenerados < 10) {
            i = random.nextInt(tam);
            j = random.nextInt(tam);
            if (!ijRepetido(i, j, vectorIJ)) {
                parIJ p = new parIJ(i, j);
                vectorIJ.add(p);
                tamVecinosGenerados++;
            }
        }
        return vectorIJ;
    }

    private boolean ijRepetido(int i, int j, Vector<parIJ> vectorIJ) {
        for (int k = 0; k < vectorIJ.size(); k++) {
            parIJ par = vectorIJ.get(k);
            if ((par.getI() == i && par.getJ() == j) || (par.getI() == j && par.getJ() == i)) {
                return true;
            }
        }
        return false;
    }

    class parIJ {

        private int i;
        private int j;

        public parIJ(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final parIJ other = (parIJ) obj;
            if (this.i != other.i) {
                return false;
            }
            if (this.j != other.j) {
                return false;
            }
            return true;
        }
    }

    private void print(int tam, int[] v) {
        for (int i = 0; i < tam; i++) {
            System.out.print(v[i] + " ");
        }
        System.out.println();
    }
}
