Solucion: 7, 2, 4, 19, 11, 1, 13, 15, 5, 14, 17, 9, 0, 10, 3, 6, 16, 8, 12, 18 
El coste de la solucion inicial es 35059
---------------------
Movimiento: 18 0
Iteracion 0
Coste mejor: 35059 
Se acepta solucion con coste 34663
---------------------
Movimiento: 13 1
Iteracion 1
Coste mejor: 34663 
Se acepta solucion con coste 33967
---------------------
Movimiento: 2 19
Iteracion 2
Coste mejor: 33967 
Se acepta solucion con coste 33680
---------------------
Movimiento: 7 5
Iteracion 3
Coste mejor: 33680 
Se acepta solucion con coste 33649
---------------------
Movimiento: 15 0
Iteracion 4
Coste mejor: 33649 
Se acepta solucion con coste 33489
---------------------
Movimiento: 15 1
Iteracion 5
Coste mejor: 33489 
Se acepta solucion con coste 33372
---------------------
Movimiento: 18 10
Iteracion 6
Coste mejor: 33372 
Se acepta solucion con coste 33008
---------------------
Movimiento: 16 1
Iteracion 7
Coste mejor: 33008 
Se acepta solucion con coste 32948
---------------------
Movimiento: 15 3
Iteracion 8
Coste mejor: 32948 
Se acepta solucion con coste 32871
---------------------
Movimiento: 7 12
Iteracion 9
Coste mejor: 32871 
Se acepta solucion con coste 32848
---------------------
Movimiento: 1 12
Iteracion 10
Coste mejor: 32848 
Se acepta solucion con coste 32605
---------------------
Movimiento: 12 18
Iteracion 11
Coste mejor: 32605 
Se acepta solucion con coste 32322
---------------------
Movimiento: 18 2
Iteracion 12
Coste mejor: 32322 
Se acepta solucion con coste 32272
---------------------
Movimiento: 14 5
Iteracion 13
Coste mejor: 32272 
Se acepta solucion con coste 32264
---------------------
Movimiento: 8 12
Iteracion 14
Coste mejor: 32264 
Se acepta solucion con coste 32047
---------------------
Movimiento: 7 12
Iteracion 15
Coste mejor: 32047 
Se acepta solucion con coste 32001
---------------------
Movimiento: 3 2
Iteracion 16
Coste mejor: 32001 
Se acepta solucion con coste 31899

El costo final es 31899
 Duracion  0.484 segundos