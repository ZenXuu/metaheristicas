Solucion: 2, 5, 15, 14, 16, 17, 18, 8, 19, 4, 7, 12, 10, 6, 0, 3, 21, 11, 1, 13, 20, 9 
El coste de la solucion inicial es 5198
---------------------
Movimiento: 0 3
Iteracion 0
Coste mejor: 5198 
Se acepta solucion con coste 5020
---------------------
Movimiento: 10 7
Iteracion 1
Coste mejor: 5020 
Se acepta solucion con coste 4824
---------------------
Movimiento: 10 4
Iteracion 2
Coste mejor: 4824 
Se acepta solucion con coste 4744
---------------------
Movimiento: 7 11
Iteracion 3
Coste mejor: 4744 
Se acepta solucion con coste 4688
---------------------
Movimiento: 1 4
Iteracion 4
Coste mejor: 4688 
Se acepta solucion con coste 4614
---------------------
Movimiento: 20 18
Iteracion 5
Coste mejor: 4614 
Se acepta solucion con coste 4530
---------------------
Movimiento: 3 21
Iteracion 6
Coste mejor: 4530 
Se acepta solucion con coste 4410
---------------------
Movimiento: 14 6
Iteracion 7
Coste mejor: 4410 
Se acepta solucion con coste 4370
---------------------
Movimiento: 15 21
Iteracion 8
Coste mejor: 4370 
Se acepta solucion con coste 4238
---------------------
Movimiento: 0 21
Iteracion 9
Coste mejor: 4238 
Se acepta solucion con coste 4140
---------------------
Movimiento: 4 7
Iteracion 10
Coste mejor: 4140 
Se acepta solucion con coste 4088
---------------------
Movimiento: 18 6
Iteracion 11
Coste mejor: 4088 
Se acepta solucion con coste 4060
---------------------
Movimiento: 11 12
Iteracion 12
Coste mejor: 4060 
Se acepta solucion con coste 4012
---------------------
Movimiento: 9 21
Iteracion 13
Coste mejor: 4012 
Se acepta solucion con coste 3988
---------------------
Movimiento: 6 7
Iteracion 14
Coste mejor: 3988 
Se acepta solucion con coste 3972
---------------------
Movimiento: 20 10
Iteracion 15
Coste mejor: 3972 
Se acepta solucion con coste 3952
---------------------
Movimiento: 1 2
Iteracion 16
Coste mejor: 3952 
Se acepta solucion con coste 3936
---------------------
Movimiento: 16 5
Iteracion 17
Coste mejor: 3936 
Se acepta solucion con coste 3932
---------------------
Movimiento: 17 18
Iteracion 18
Coste mejor: 3932 
Se acepta solucion con coste 3914
---------------------
Movimiento: 7 4
Iteracion 19
Coste mejor: 3914 
Se acepta solucion con coste 3874
---------------------
Movimiento: 7 6
Iteracion 20
Coste mejor: 3874 
Se acepta solucion con coste 3846
---------------------
Movimiento: 16 17
Iteracion 21
Coste mejor: 3846 
Se acepta solucion con coste 3834

El costo final es 3834
 Duracion  0.744 segundos