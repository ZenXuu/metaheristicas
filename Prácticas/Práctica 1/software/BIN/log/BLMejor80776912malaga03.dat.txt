Solucion: 9, 14, 12, 22, 27, 0, 17, 3, 7, 13, 4, 18, 11, 10, 29, 28, 26, 25, 8, 24, 16, 6, 1, 20, 23, 2, 15, 5, 21, 19 
El coste de la solucion inicial es 13824
---------------------
Movimiento: 9 5
Iteracion 0
Coste mejor: 13824 
Se acepta solucion con coste 13765
---------------------
Movimiento: 12 6
Iteracion 1
Coste mejor: 13765 
Se acepta solucion con coste 13743
---------------------
Movimiento: 22 18
Iteracion 2
Coste mejor: 13743 
Se acepta solucion con coste 13707
---------------------
Movimiento: 22 10
Iteracion 3
Coste mejor: 13707 
Se acepta solucion con coste 13700
---------------------
Movimiento: 8 11
Iteracion 4
Coste mejor: 13700 
Se acepta solucion con coste 13679
---------------------
Movimiento: 27 10
Iteracion 5
Coste mejor: 13679 
Se acepta solucion con coste 13672
---------------------
Movimiento: 13 28
Iteracion 6
Coste mejor: 13672 
Se acepta solucion con coste 13670
---------------------
Movimiento: 0 18
Iteracion 7
Coste mejor: 13670 
Se acepta solucion con coste 13662
---------------------
Movimiento: 20 13
Iteracion 8
Coste mejor: 13662 
Se acepta solucion con coste 13654
---------------------
Movimiento: 18 13
Iteracion 9
Coste mejor: 13654 
Se acepta solucion con coste 13650
---------------------
Movimiento: 13 27
Iteracion 10
Coste mejor: 13650 
Se acepta solucion con coste 13646
---------------------
Movimiento: 7 21
Iteracion 11
Coste mejor: 13646 
Se acepta solucion con coste 13639
---------------------
Movimiento: 18 29
Iteracion 12
Coste mejor: 13639 
Se acepta solucion con coste 13635
---------------------
Movimiento: 2 17
Iteracion 13
Coste mejor: 13635 
Se acepta solucion con coste 13633
---------------------
Movimiento: 7 2
Iteracion 14
Coste mejor: 13633 
Se acepta solucion con coste 13627
---------------------
Movimiento: 11 19
Iteracion 15
Coste mejor: 13627 
Se acepta solucion con coste 13613
---------------------
Movimiento: 0 29
Iteracion 16
Coste mejor: 13613 
Se acepta solucion con coste 13602
---------------------
Movimiento: 23 8
Iteracion 17
Coste mejor: 13602 
Se acepta solucion con coste 13589
---------------------
Movimiento: 3 26
Iteracion 18
Coste mejor: 13589 
Se acepta solucion con coste 13579
---------------------
Movimiento: 8 6
Iteracion 19
Coste mejor: 13579 
Se acepta solucion con coste 13565
---------------------
Movimiento: 18 5
Iteracion 20
Coste mejor: 13565 
Se acepta solucion con coste 13551
---------------------
Movimiento: 2 9
Iteracion 21
Coste mejor: 13551 
Se acepta solucion con coste 13550
---------------------
Movimiento: 18 24
Iteracion 22
Coste mejor: 13550 
Se acepta solucion con coste 13537
---------------------
Movimiento: 24 12
Iteracion 23
Coste mejor: 13537 
Se acepta solucion con coste 13525
---------------------
Movimiento: 6 10
Iteracion 24
Coste mejor: 13525 
Se acepta solucion con coste 13524
---------------------
Movimiento: 25 9
Iteracion 25
Coste mejor: 13524 
Se acepta solucion con coste 13520
---------------------
Movimiento: 11 5
Iteracion 26
Coste mejor: 13520 
Se acepta solucion con coste 13515
---------------------
Movimiento: 25 18
Iteracion 27
Coste mejor: 13515 
Se acepta solucion con coste 13513
---------------------
Movimiento: 6 9
Iteracion 28
Coste mejor: 13513 
Se acepta solucion con coste 13504
---------------------
Movimiento: 22 12
Iteracion 29
Coste mejor: 13504 
Se acepta solucion con coste 13489
---------------------
Movimiento: 22 17
Iteracion 30
Coste mejor: 13489 
Se acepta solucion con coste 13486
---------------------
Movimiento: 29 23
Iteracion 31
Coste mejor: 13486 
Se acepta solucion con coste 13475
---------------------
Movimiento: 6 18
Iteracion 32
Coste mejor: 13475 
Se acepta solucion con coste 13474
---------------------
Movimiento: 6 11
Iteracion 33
Coste mejor: 13474 
Se acepta solucion con coste 13473
---------------------
Movimiento: 17 11
Iteracion 34
Coste mejor: 13473 
Se acepta solucion con coste 13469
---------------------
Movimiento: 22 12
Iteracion 35
Coste mejor: 13469 
Se acepta solucion con coste 13464
---------------------
Movimiento: 12 23
Iteracion 36
Coste mejor: 13464 
Se acepta solucion con coste 13463
---------------------
Movimiento: 6 11
Iteracion 37
Coste mejor: 13463 
Se acepta solucion con coste 13459

El costo final es 13459
 Duracion  0.544 segundos