Solucion: 14, 9, 4, 12, 3, 5, 8, 13, 19, 17, 6, 7, 1, 16, 15, 18, 2, 11, 0, 10 
El coste de la solucion inicial es 3470
---------------------
Movimiento: 13 19
Iteracion 0
Coste mejor: 3470 
Se acepta solucion con coste 3378
---------------------
Movimiento: 3 6
Iteracion 1
Coste mejor: 3378 
Se acepta solucion con coste 3354
---------------------
Movimiento: 8 10
Iteracion 2
Coste mejor: 3354 
Se acepta solucion con coste 3194
---------------------
Movimiento: 9 4
Iteracion 3
Coste mejor: 3194 
Se acepta solucion con coste 3136
---------------------
Movimiento: 0 5
Iteracion 4
Coste mejor: 3136 
Se acepta solucion con coste 3052
---------------------
Movimiento: 15 8
Iteracion 5
Coste mejor: 3052 
Se acepta solucion con coste 3010
---------------------
Movimiento: 0 6
Iteracion 6
Coste mejor: 3010 
Se acepta solucion con coste 3006
---------------------
Movimiento: 1 2
Iteracion 7
Coste mejor: 3006 
Se acepta solucion con coste 2976
---------------------
Movimiento: 3 19
Iteracion 8
Coste mejor: 2976 
Se acepta solucion con coste 2972
---------------------
Movimiento: 9 7
Iteracion 9
Coste mejor: 2972 
Se acepta solucion con coste 2964
---------------------
Movimiento: 18 7
Iteracion 10
Coste mejor: 2964 
Se acepta solucion con coste 2952
---------------------
Movimiento: 12 13
Iteracion 11
Coste mejor: 2952 
Se acepta solucion con coste 2928
---------------------
Movimiento: 16 5
Iteracion 12
Coste mejor: 2928 
Se acepta solucion con coste 2922
---------------------
Movimiento: 4 19
Iteracion 13
Coste mejor: 2922 
Se acepta solucion con coste 2910
---------------------
Movimiento: 8 19
Iteracion 14
Coste mejor: 2910 
Se acepta solucion con coste 2886
---------------------
Movimiento: 5 3
Iteracion 15
Coste mejor: 2886 
Se acepta solucion con coste 2842
---------------------
Movimiento: 15 10
Iteracion 16
Coste mejor: 2842 
Se acepta solucion con coste 2822
---------------------
Movimiento: 16 17
Iteracion 17
Coste mejor: 2822 
Se acepta solucion con coste 2816
---------------------
Movimiento: 14 9
Iteracion 18
Coste mejor: 2816 
Se acepta solucion con coste 2812
---------------------
Movimiento: 5 6
Iteracion 19
Coste mejor: 2812 
Se acepta solucion con coste 2770
---------------------
Movimiento: 17 18
Iteracion 20
Coste mejor: 2770 
Se acepta solucion con coste 2764
---------------------
Movimiento: 1 5
Iteracion 21
Coste mejor: 2764 
Se acepta solucion con coste 2756
---------------------
Movimiento: 17 11
Iteracion 22
Coste mejor: 2756 
Se acepta solucion con coste 2740
---------------------
Movimiento: 16 17
Iteracion 23
Coste mejor: 2740 
Se acepta solucion con coste 2718
---------------------
Movimiento: 5 6
Iteracion 24
Coste mejor: 2718 
Se acepta solucion con coste 2706
---------------------
Movimiento: 0 1
Iteracion 25
Coste mejor: 2706 
Se acepta solucion con coste 2702
---------------------
Movimiento: 10 11
Iteracion 26
Coste mejor: 2702 
Se acepta solucion con coste 2698
---------------------
Movimiento: 1 0
Iteracion 27
Coste mejor: 2698 
Se acepta solucion con coste 2682

El costo final es 2682
 Duracion  0.795 segundos